package ai.ecma.apporderservice.controller;

import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.bonus.*;
import ai.ecma.apporderservice.utils.RestConstants;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstants.PRODUCT_PRICE_BONUS_CONTROLLER)
public interface ProductPriceBonusController {
    @GetMapping("/{id}")
    ApiResult<ProductPriceBonusBasketDto> getForBasket(@PathVariable UUID id);

    @GetMapping("/get-box-name/{productPriceBonusId}")
    ApiResult<List<BoxProductForProductPriceBonusDto>> getBoxProductByProductPriceBonus(@PathVariable UUID productPriceBonusId);

    @GetMapping("/box-products/{productPriceBonusId}")
    ApiResult<List<ProductForBoxProductDto>> getProductForBoxProductClient(@PathVariable UUID id, @PathVariable String name);

    @PostMapping
    ApiResult<ProductPriceBonusGetSelectDto> add(@RequestBody @Valid ProductPriceBonusAddDto productPriceBonusAddDto);

    @PutMapping("/{id}")
   ApiResult<ProductPriceBonusForAdminDto> edit(@PathVariable UUID id, @RequestBody @Valid ProductPriceBonusAddDto productPriceBonusAddDto);
    @DeleteMapping("/{id}")
    ApiResult<?> delete(@PathVariable UUID id);
}
