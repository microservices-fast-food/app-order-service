package ai.ecma.apporderservice.mapper;

import ai.ecma.appdblib.entity.enums.BonusTypeEnum;
import ai.ecma.appdblib.entity.order.bonus.SaleBonus;
import ai.ecma.apporderservice.payload.bonus.*;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring",imports = BonusTypeEnum.class)
public interface SaleBonusMapper {

    // Ushbu method saleBonus dan saleBonusResDto yasab beradi
    SaleBonusResDto saleBonusResDto(SaleBonus saleBonus);

    // Ushbu method saleBonusReqDto dan saleBonus yasab beradi
    @Mapping(target = "type",expression = "java(BonusTypeEnum.SALE_BONUS)")
    SaleBonus saleBonusReqDtoToSaleBonus(SaleBonusReqDto saleBonusReqDto);

    // Ushbu metod saleBonusReqDto dan olib saleBonus ga qiymatlarini qo'yib beradi
    void updateSaleBonus(SaleBonusReqDto saleBonusReqDto, @MappingTarget SaleBonus saleBonus);

    // Ushbu method saleBonus dan saleBonusViewDto yasab beradi
    @Mapping(target = "type",expression = "java(BonusTypeEnum.SALE_BONUS)")
    @Mapping(target = "sale", expression = "java(sale)")
    SaleBonusViewDto saleBonusToSaleBonusViewDto(SaleBonus saleBonus, @Context SaleResDto sale);



    SaleBonusViewDto saleBonusToSaleBonusViewDto(SaleBonus saleBonus);

    SaleBonusBasketDto saleBonusToSaleBonusBasketDto(SaleBonusResDto saleBonusResDto);
}
