package ai.ecma.apporderservice.payload.bonus;

import java.util.UUID;

public class BoxProductResDto {
    private UUID id;

    private UUID productId;

    private String boxName;

    private UUID productPriceBonusId;
}
