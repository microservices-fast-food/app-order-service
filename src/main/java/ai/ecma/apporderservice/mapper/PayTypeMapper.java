package ai.ecma.apporderservice.mapper;

import ai.ecma.appdblib.entity.order.PayType;
import ai.ecma.apporderservice.payload.PayTypeResDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PayTypeMapper {
    List<PayTypeResDto> payTypeListToResDto(List<PayType> payTypeList);

    PayTypeResDto payTypeToResDto(PayType payType);

}
