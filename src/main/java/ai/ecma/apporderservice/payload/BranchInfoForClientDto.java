package ai.ecma.apporderservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BranchInfoForClientDto {
    private UUID id;
    private Double distance;
    private Double deliveryPrice;
    private Integer cookingTime;
}
