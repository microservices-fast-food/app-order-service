package ai.ecma.apporderservice.controller;

import ai.ecma.appdblib.entity.enums.OrderStatusEnum;
import ai.ecma.apporderservice.payload.*;
import ai.ecma.apporderservice.utils.AppConstant;
import ai.ecma.apporderservice.utils.RestConstants;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstants.ORDER_CONTROLLER)
public interface OrderController {
    String ADD_OR_REMOVE_PRODUCT = "/add-or-remove-product";
    String UPDATE_ORDER_PRODUCT_PRICE = "/update-price-order-product";
    String ADD_BOX_TO_ORDER = "/add-box-to-order";
    String CLEAR_BASKET = "/clear-basket";
    String ORDER_CONFIRM = "/order-confirm";
    String ORDER_SELECT_BRANCH = "/order-select-branch";
    String GET_ORDERS_BY_BRANCH = "/get-orders-by-branch";
    String ADD_COURIER_TO_ORDER = "/add-courier-to-order";

    String CANCEL_ORDER_BY_CUSTOMER = "/cancel-order-by-customer";
    String GET_ORDER_FOR_COURIER = "/get-order-for-courier";
    String REJECT_ORDER_BY_KITCHEN = "/reject-order-by-kitchen";
    String VIEW_ORDERS_BY_KITCHEN = "/view-orders-by-kitchen";
    String COURIER_CHANGE_ORDER_STATUS_TO_CLOSED = "/courier-change-order-status-to-closed";
    String VIEW_ORDERS_FOR_CLIENT = "/view-orders-for-client";
    String ADD_ORDER_AND_DELIVERY_RATE = "/add-order-and-delivery-rate";
    String GET_FULL_DATA_ORDER_CLIENT = "/get-full-data-order-client";
    String READY_ORDER_BY_KITCHEN="/ready-order-by-kitchen";
    String HISTORY_ORDERS_FOR_COURIER="/history-orders-for-courier";
    String EDIT_ORDER_STATUS="/edit-order-status";


    @PostMapping(ADD_OR_REMOVE_PRODUCT)
    ApiResult<OrderBasketDto> addOrRemoveProduct(@RequestBody @Valid AddOrRemoveProductDto addOrREmoveProductDto);

    //mahsulot narxi o'zgarganda shu yo'lsa kelib  order statusi draft bo'ganlarini  shu mahsulotni yangi narxini qo'yib qayta hisoblaydi
    @PostMapping(UPDATE_ORDER_PRODUCT_PRICE)
    ApiResult<?> updateOrderProductPrice(@RequestBody OrderUpdatePriceDto orderUpdatePriceDto);

    @PostMapping(ADD_BOX_TO_ORDER + "/{boxName}")
    ApiResult<?> addBoxNameToOrder(@PathVariable String boxName);

    @DeleteMapping(CLEAR_BASKET)
    ApiResult<?> clearBasket();

    @PostMapping(ORDER_CONFIRM)
    ApiResult<?> orderConfirm(@RequestBody @Valid OrderConfirmDto orderConfirmDto);

    @PostMapping(ORDER_SELECT_BRANCH)
    ApiResult<List<BranchInfoForClientDto>> orderSelectBranch(@RequestBody @Valid FindNearestBranchDto findNearestBranchDto);

    @PutMapping(COURIER_CHANGE_ORDER_STATUS_TO_CLOSED + "/{courierOrderId}")
    ApiResult<?> courierChangeOrderStatusToClosed(@PathVariable UUID courierOrderId);


    @PutMapping("/{orderId}")
    ApiResult<?> orderEditByOperator(@RequestBody OrderStatusEnum status, @PathVariable UUID orderId);

    @GetMapping
    ApiResult<CustomPage<OrderForOperatorDto>> getAllForOperator(int page, int size);

    @PutMapping(CANCEL_ORDER_BY_CUSTOMER + "{orderId}")
    ApiResult<?> cancelOrderByCustomer(@PathVariable UUID orderId);

    /**
     * Dispetcher uchun branch ga tegishli courier biriktirilmagan va status KITCHEN bo'lgan barcha order lani olish
     * @param branchId
     * @return
     */
    @GetMapping(GET_ORDERS_BY_BRANCH + "/{branchId}")
    ApiResult<List<OrderForDispatcherDto>> getOrdersByBranchId(@PathVariable UUID branchId);

    /**
     * Bo'sh courier ga yetkazilishi kerak bo'lgan order ni biriktirish
     * @param addCourierToOrderDto
     * @return
     */
    @PutMapping(ADD_COURIER_TO_ORDER)
    ApiResult<String> addCourierToOrder(@RequestBody @Valid AddCourierToOrderDto addCourierToOrderDto);

    @GetMapping(GET_ORDER_FOR_COURIER)
    ApiResult<CourierOrderBoxDto> getOrderForCourier();

    @PutMapping(REJECT_ORDER_BY_KITCHEN+"{orderId}")
    ApiResult<?> rejectOrderByKitchen(@PathVariable UUID orderId);

    @PutMapping(READY_ORDER_BY_KITCHEN+"{orderId}")
    ApiResult<?> readyOrderByKitchen(@PathVariable UUID orderId);

    @PutMapping(VIEW_ORDERS_BY_KITCHEN+"{branchId}")
    ApiResult<?> viewOrdersByKitchen(@PathVariable UUID branchId);

    @GetMapping(VIEW_ORDERS_FOR_CLIENT)
    ApiResult<CustomPage<ViewOrdersForClientDto>> viewOrdersForClient(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                                                      @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @PostMapping(ADD_ORDER_AND_DELIVERY_RATE)
    ApiResult<String> addOrderAndDeliveryRate(@RequestBody @Valid AddOrderDeliveryRate addOrderDeliveryRate);

    @GetMapping(GET_FULL_DATA_ORDER_CLIENT+"/{orderId}")
    ApiResult<FullDataOrderForClientDto> getFullDataOrderForClient(@PathVariable UUID orderId);


    @GetMapping(HISTORY_ORDERS_FOR_COURIER)
    ApiResult<List<CourierOrderBoxDto>> getHistoryOrderListForCourier();

    @PutMapping(EDIT_ORDER_STATUS+"/{orderId}")
    ApiResult<String> editOrderStatus(@PathVariable UUID orderId);

}
