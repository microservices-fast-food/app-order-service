package ai.ecma.apporderservice.payload.bonus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.sql.Time;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductCountBonusAddDto {
    @NotBlank(message = "Name bo'sh bo'lmasligi kerak")
    private String name;

    @NotNull(message = "Start time bo'sh bo'lmasligi kerak")
    private Time startTime;

    @NotNull(message = "Start time bo'sh bo'lmasligi kerak")
    private Time endTime;

    @FutureOrPresent(message = "Boshlanish sanasi hozirgi yoki kelasi sana bo'lshi kerak")
    @NotNull(message = "Start date bo'sh bo'lmasligi kerak")
    private Date startDate;

    @Future(message = "Tugash sanasi hozirgi sanadan keyin bo'lishi kerak")
    @NotNull(message = "End date bo'sh bo'lmasligi kerak")
    private Date endDate;

    @NotNull(message = "Mahsulot bo'sh bo'lmasligi kerak")
    private UUID productId;

    @NotNull(message = "Mahsulot soni bo'sh bo'lmasligi kerak")
    private Integer productCount;

    @NotNull(message = "Sovg'a mahsulot bo'sh bo'lmasligi kerak")
    private UUID giftProductId;

    @NotNull(message = "Sovg'a mahsulot soni bo'sh bo'lmasligi kerak")
    private Integer giftProductCount;
}
