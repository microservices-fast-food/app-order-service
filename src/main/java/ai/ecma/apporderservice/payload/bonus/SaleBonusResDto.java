package ai.ecma.apporderservice.payload.bonus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaleBonusResDto {
    private UUID id;
    private String name;
    private Double minPrice;
    private UUID saleId;
}
