package ai.ecma.apporderservice.controller;

import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.PayDto;
import ai.ecma.apporderservice.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class PaymentControllerImpl implements PaymentController{

    private final PaymentService paymentService;
    @Override
    public ApiResult<?> paymentProcess(PayDto payDto) {
        return paymentService.paymentProcess(payDto);
    }
}
