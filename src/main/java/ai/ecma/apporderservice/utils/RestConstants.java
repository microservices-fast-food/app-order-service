package ai.ecma.apporderservice.utils;

public interface RestConstants {
    String BASE_PATH = "/api/open-order";
    String DOMAIN = "http://localhost";
    String COMMENT_CONTROLLER =  BASE_PATH + "/comment";
    String SALE_BONUS_CONTROLLER =  BASE_PATH + "/sale-bonus";
    String PRODUCT_SERVICE = "PRODUCT-SERVICE/api/open-product";
    String ATTACHMENT_CONTROLLER = PRODUCT_SERVICE+"/attachment";
    String PRODUCT_CONTROLLER = PRODUCT_SERVICE+"/product";
    String PRODUCT_PRICE_BONUS_CONTROLLER = BASE_PATH + "/product-price-bonus";
    String PRODUCT_COUNT_BONUS_CONTROLLER = BASE_PATH + "/product-count-bonus";
    String REQUEST_ATTRIBUTE_CURRENT_USER = "user";
    String AUTH_SERVICE = "AUTH-SERVICE/api/open-auth";
    String DELIVERY_FREE_BONUS_CONTROLLER= BASE_PATH +"/delivery-free-bonus";
    String ORDER_CONTROLLER= BASE_PATH +"/order";
    String PAY_TYPE_CONTROLLER= BASE_PATH +"/pay-type";
    String PAYMENT_CONTROLLER= BASE_PATH + "/payment";
}
