package ai.ecma.apporderservice.feign;

import ai.ecma.appdblib.entity.product.Attachment;
import ai.ecma.apporderservice.config.FeignConfig;
import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.utils.RestConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;

@FeignClient(name = RestConstants.ATTACHMENT_CONTROLLER, configuration = FeignConfig.class)
public interface AttachmentFeign {
    @GetMapping("/info/{id}")
    ApiResult<Attachment> getAttachmentById(@PathVariable(name = "id") UUID id);
}
