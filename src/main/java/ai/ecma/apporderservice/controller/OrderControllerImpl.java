package ai.ecma.apporderservice.controller;

import ai.ecma.appdblib.entity.enums.OrderStatusEnum;
import ai.ecma.apporderservice.payload.*;
import ai.ecma.apporderservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@Slf4j
public class OrderControllerImpl implements OrderController{
    private final OrderService orderService;

    @Override
    public ApiResult<OrderBasketDto> addOrRemoveProduct(AddOrRemoveProductDto addOrRemoveProductDto) {
        log.info("OrderController addOrRemoveProduct req addOrRemoveProductDto :{}" ,addOrRemoveProductDto);
        ApiResult<OrderBasketDto> apiResult= orderService.addOrRemoveProduct(addOrRemoveProductDto);
        log.info("OrderController addOrRemoveProduct resp apiResult :{}" ,apiResult.getData());
        return apiResult;
    }

    @Override
    public ApiResult<?> updateOrderProductPrice(OrderUpdatePriceDto orderUpdatePriceDto) {
        log.info("OrderController updateOrderProductPrice req orderUpdatePriceDto :{}", orderUpdatePriceDto);
        ApiResult<?> apiResult = orderService.updateOrderProductPrice(orderUpdatePriceDto);
        log.info("OrderController updateOrderProductPrice resp apiResult :{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<?> addBoxNameToOrder(String boxName) {
        log.info("OrderController addBoxNameToOrder req boxName:{}", boxName);
        ApiResult<?> apiResult = orderService.addBoxNameToOrder(boxName);
        log.info("OrderController addBoxNameToOrder resp apiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<?> courierChangeOrderStatusToClosed(UUID courierOrderId) {
        return orderService.courierChangeOrderStatusToClosed( courierOrderId);
    }

    @Override
    public ApiResult<?> clearBasket() {
        log.info("OrderController clearBasket req ");
        ApiResult<?> apiResult = orderService.clearBasket();
        log.info("OrderController clearBasket resp apiResult :{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<?> orderConfirm(OrderConfirmDto orderConfirmDto) {
        log.info("OrderController orderConfirm req orderConfirmDto:{}", orderConfirmDto);
        ApiResult<?> apiResult = orderService.orderConfirm(orderConfirmDto);
        log.info("OrderController orderConfirm resp apiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<List<BranchInfoForClientDto>> orderSelectBranch(FindNearestBranchDto findNearestBranchDto) {
        log.info("OrderController orderSelectBranch req findNearestBranchDto:{}", findNearestBranchDto);
        ApiResult<List<BranchInfoForClientDto>> apiResult = orderService.orderSelectBranch(findNearestBranchDto);
        log.info("OrderController orderSelectBranch resp apiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<?> orderEditByOperator(OrderStatusEnum status, UUID orderId) {
        log.info("OrderController orderEditByOperator req status:{}, orderId:{}", status, orderId);
        ApiResult<?> apiResult = orderService.orderEditByOperator(status, orderId);
        log.info("OrderController orderEditByOperator resp apiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<CustomPage<OrderForOperatorDto>> getAllForOperator(int page, int size) {
        log.info("OrderController getAllForOperator req page:{}, size:{}", page, size);
        ApiResult<CustomPage<OrderForOperatorDto>> apiResult = orderService.getAllForOperator(page, size);
        log.info("OrderController getAllForOperator resp apiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<?> cancelOrderByCustomer(UUID orderId) {
        log.info("OrderController cancelOrderByCustomer req orderId:{}", orderId);
        ApiResult<?> apiResult = orderService.cancelOrderByCustomer(orderId);
        log.info("OrderController cancelOrderByCustomer resp apiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<CourierOrderBoxDto> getOrderForCourier() {
        log.info("OrderController getOrderForCourier req ");
        ApiResult<CourierOrderBoxDto> apiResult = orderService.getOrderForCourier();
        log.info("OrderController getOrderForCourier resp apiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<?> rejectOrderByKitchen(UUID orderId) {
        log.info("OrderController rejectOrderByKitchen req orderId:{}", orderId);
        ApiResult<?> apiResult = orderService.rejectOrderByKitchen(orderId);
        log.info("OrderController rejectOrderByKitchen resp apiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<?> readyOrderByKitchen(UUID orderId) {
        return orderService.readyOrderByKitchen(orderId);
    }

    @Override
    public ApiResult<FullDataOrderForClientDto> getFullDataOrderForClient(UUID orderId) {
        return orderService.getFullDataOrderForClient(orderId);
    }

    @Override
    public ApiResult<String> addOrderAndDeliveryRate(AddOrderDeliveryRate addOrderDeliveryRate) {
        return orderService.addOrderAndDeliveryRate(addOrderDeliveryRate);
    }

    @Override
    public ApiResult<CustomPage<ViewOrdersForClientDto>> viewOrdersForClient(int page, int size) {
        return orderService.viewOrdersForClient(page, size);
    }

    @Override
    public ApiResult<?> viewOrdersByKitchen(UUID branchId) {
        log.info("OrderController viewOrdersByKitchen req branchId:{}", branchId);
        ApiResult<?> apiResult = orderService.viewOrdersByKitchen(branchId);
        log.info("OrderController viewOrdersByKitchen resp apiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<List<CourierOrderBoxDto>> getHistoryOrderListForCourier() {
        return orderService.getHistoryOrderListForCourier();
    }

    @Override
    public ApiResult<String> editOrderStatus(UUID orderId) {
        return orderService.editOrderStatus(orderId);
    }

    @Override
    public ApiResult<List<OrderForDispatcherDto>> getOrdersByBranchId(UUID branchId) {

        return orderService.getOrdersByBranchId(branchId);
    }

    @Override
    public ApiResult<String> addCourierToOrder(AddCourierToOrderDto addCourierToOrderDto) {
        return orderService.addCourierToOrder(addCourierToOrderDto);
    }


}
