package ai.ecma.apporderservice.payload.bonus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.sql.Time;

@AllArgsConstructor
@NoArgsConstructor
@Data
// Yetkazib berish narxini qo'shish uchun ishlatiladi
public class DeliveryFreeBonusAddDto {

    @NotBlank(message = "Yetkazib berish narxi bo'sh bo'lmasligi kerak")
    private String name;

    @NotNull(message = "Boshlanish vaqti bo'sh bo'lmasligi kerak")
    private Time startTime;

    @NotNull(message = "Tugash vaqti bo'sh bo'lmasligi kerak")
    private Time endTime;

    @FutureOrPresent(message = "Boshlanish sanasi hozirgi yoki kelasi sana bo'lshi kerak")
    @NotNull(message = "Boshlanish kuni bo'sh bo'lmasligi kerak")
    private Date startDate;

    @Future(message = "Berilgan vaqt hozirgi vaqtdan keyin bo'lishi kerak")
    @NotNull(message = "Tugash kuni bo'sh bo'lmasligi kerak")
    private Date endDate;

    @NotNull(message = "MinNarxi bo'sh bo'lmasligi kerak")
    private Double minPrice;
}
