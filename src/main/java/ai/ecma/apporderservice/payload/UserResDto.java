package ai.ecma.apporderservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResDto {
    private UUID id;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private Date birthdate;

    private UUID avatarId;

    private String avatarUrl;

}
