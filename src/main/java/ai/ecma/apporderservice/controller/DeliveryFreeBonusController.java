package ai.ecma.apporderservice.controller;

import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.CustomPage;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusAddDto;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusBasketDto;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusGetForAdmin;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeEditDto;
import ai.ecma.apporderservice.utils.RestConstants;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

import static ai.ecma.apporderservice.utils.AppConstant.DEFAULT_PAGE_NUMBER;
import static ai.ecma.apporderservice.utils.AppConstant.DEFAULT_PAGE_SIZE;

@Api(value = "DeliveriFreeBonus")
@RequestMapping(RestConstants.DELIVERY_FREE_BONUS_CONTROLLER)
public interface DeliveryFreeBonusController {

    @GetMapping
    ApiResult<CustomPage<DeliveryFreeBonusBasketDto>> getAll(@RequestParam(defaultValue = DEFAULT_PAGE_NUMBER) int page,
                                                             @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) int size);

     /**
     *  @param page RequestParam
     *  @param size RequestParam
     *  @return ApiResult<CustomPage<DeliveryFreeBonusGetForAdmin>>
     *  maqsad:Adminga hamma yetkazib berish narxlari page orqali ko'rinishi kerak.
     *  jarayonlar:
      *  1.1. Pageable orqali page va sizini ovolamiz.
      *  1.2. DeliveryFreeBonusRepository dan findAll ga yasagan pageableni beramiz va
      *  deliveryFreeBonusPage o'zgaruvchiga ovolamiz.
      *  1.3. CustomPage yasab deliveryFreeBonusPage ni bervoramiz va buni ham bitta o'zgaruvchiga ovolamiz.
      *  1.4. ApiResult ga yasagan customPageni beramiz va metodni tugallaymiz.
     */
    @GetMapping("/get-all-for-admin")
    ApiResult<CustomPage<DeliveryFreeBonusGetForAdmin>> getForAdmin(@RequestParam(defaultValue = DEFAULT_PAGE_NUMBER) int page,
                                                        @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) int size);

    /**
     *  @param id DeliveryFreeBonusning idisi
     *  @return
     *  maqsad:id orqali har bir yetkazib berish narxlarini ko'rish mumkun
     *  jarayonlar:
     *  1.1. DeliveryFreeBonusRepository ga id orqali boglanamiz:
     *      1.1.1. Agar DeliveryFreeBonusRepository dan id orqali hech narsa topaolmasa throw.
     *      1.1.2. Agar DeliveryFreeBonusRepository dan id orqali deliveryFreeBonus topsa davom etamiz.
     *  1.2. DeliveryFreeBonusMapper da deliveryFreeBonusToResDto yasab id orqali topgan deliveryFreeBonus
     *  bervoramiz.
     *  1.3. ApiResult ga yasagan deliveryFreeBonus beramiz va metodni tugallaymiz.
     */
    @GetMapping("{/id}")
    ApiResult<DeliveryFreeBonusBasketDto> getOne(@PathVariable UUID id);

    /**
     *  @param deliveryFreeBonusAddDto
     *  @return
     *  maqsad:Adminga hamma yetkazib berishnarxlari ko'rnishi kerak
     *  jarayonlar:
     */
    @PostMapping
    ApiResult<DeliveryFreeBonusBasketDto> add(@RequestBody @Valid DeliveryFreeBonusAddDto deliveryFreeBonusAddDto);

    @PutMapping("{/id}")
    ApiResult<DeliveryFreeBonusBasketDto> edit(@RequestBody @Valid DeliveryFreeEditDto deliveryFreeEditDto, @PathVariable UUID id);

    @DeleteMapping("{/id}")
    ApiResult<?> delete(@PathVariable UUID id);
}
