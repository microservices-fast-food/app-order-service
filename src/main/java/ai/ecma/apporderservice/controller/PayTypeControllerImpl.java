package ai.ecma.apporderservice.controller;

import ai.ecma.appdblib.entity.enums.PayTypeEnum;
import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.PayTypeResDto;
import ai.ecma.apporderservice.service.PayTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class PayTypeControllerImpl implements PayTypeController{

    private final PayTypeService payTypeService;

    @Override
    public ApiResult<List<PayTypeResDto>> getAll() {
        return payTypeService.getAll();
    }

    @Override
    public ApiResult<List<PayTypeResDto>> getFilter(PayTypeEnum type) {
        return payTypeService.getFilter(type);
    }

    @Override
    public ApiResult<PayTypeResDto> getOne(UUID id) {
        return payTypeService.getOne(id);
    }
}




