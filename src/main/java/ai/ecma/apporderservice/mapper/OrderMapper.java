package ai.ecma.apporderservice.mapper;

import ai.ecma.appdblib.entity.order.Order;
import ai.ecma.apporderservice.payload.*;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Mapper(componentModel = "spring")
public abstract class OrderMapper {
    @Autowired
    OrderBonusMapper orderBonusMapper;

    @Mapping(target = "orderBonus", expression = "java(orderBonusMapper.orderBonusToBasketDto(order.getOrderBonus()))")
    @Mapping(target = "products", expression = "java(products)")
    public abstract OrderBasketDto OrderToBasketDto(Order order,
                                                    @Context List<ProductBasketDto> products);

    //Ushbu metod Order dan OrderForOperatorDto ni yasab beradi
//    @Mapping(target = "totalPrice", expression = "java(order.getPrice()+order.getDeliveryPrice())")
    @Mapping(target = "firstName", expression = "java(user.getFirstName())")
//    @Mapping(target = "firtsName", expression = "java(user.getLastName())")
//    @Mapping(target = "firtsName", expression = "java(user.getPhoneNumber())")
    public abstract OrderForOperatorDto orderForOperatorToDto(Order order, @Context UserResDto user);

    //Ushbe metod Order listi dan OrderForOperatorDto listini yasab beradi
    public abstract List<OrderForOperatorDto> orderForOperatorDtoList(List<Order> orderList);

    @Mapping(target = "lastName", expression = "java(getLastNameInHashMap(userDtoHashMap, order.getCourierId()))")
    @Mapping(target = "phoneNumber", expression = "java(getPhoneNumberInHashMap(userDtoHashMap, order.getCourierId()))")
    @Mapping(target = "address", expression = "java(getAddressInHashMap(addressDtoHashMap, order.getAddressId()))")
    @Mapping(target = "totalPrice", expression = "java(order.getPrice()+order.getDeliveryPrice())")
    public abstract OrderForDispatcherDto orderToDispatcherDto(Order order,
                                                               @Context HashMap<UUID, UserDto> userDtoHashMap,
                                                               @Context HashMap<UUID, AddressForDispatcherDto> addressDtoHashMap);

    public abstract List<OrderForDispatcherDto> orderToDispatcherDtoList(List<Order> orders,
                                                                         @Context HashMap<UUID, UserDto> userDtoHashMap,
                                                                         @Context HashMap<UUID, AddressForDispatcherDto> addressDtoHashMap);

    @Mapping(target = "totalPrice", expression = "java(order.getPrice()+order.getDeliveryPrice())")
    public abstract ViewOrdersForClientDto orderToViewClientDto(Order order);

    public abstract List<ViewOrdersForClientDto> orderToViewClientDto(List<Order> orders);


//    @Mapping(target = "orderForCourierDtoList", expression = "java(orderForOperatorDtoList(List<Order> orderList))")
//    public abstract CourierOrderBoxDto courierOrderBoxToDto(Order order);


    AddressForDispatcherDto getAddressInHashMap(HashMap<UUID, AddressForDispatcherDto> addressMap, UUID addressId) {

        if (addressMap.containsKey(addressId))
            return addressMap.get(addressId);

        return null;
    }

    String getLastNameInHashMap(HashMap<UUID, UserDto> userDtoHashMap, UUID userId) {
        if (userDtoHashMap.containsKey(userId))
            return userDtoHashMap.get(userId).getLastName();

        return null;
    }

    String getPhoneNumberInHashMap(HashMap<UUID, UserDto> userDtoHashMap, UUID userId) {
        if (userDtoHashMap.containsKey(userId))
            return userDtoHashMap.get(userId).getPhoneNumber();

        return null;
    }


}
