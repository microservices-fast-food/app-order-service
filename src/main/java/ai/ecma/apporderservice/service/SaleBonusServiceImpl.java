package ai.ecma.apporderservice.service;


import ai.ecma.appdblib.entity.order.bonus.SaleBonus;
import ai.ecma.appdblib.repository.order.SaleBonusRepository;
import ai.ecma.apporderservice.component.MessageByLang;
import ai.ecma.apporderservice.exception.RestException;
import ai.ecma.apporderservice.feign.ProductFeign;
import ai.ecma.apporderservice.mapper.SaleBonusMapper;
import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.bonus.SaleBonusReqDto;
import ai.ecma.apporderservice.payload.bonus.SaleBonusResDto;
import ai.ecma.apporderservice.payload.bonus.SaleBonusViewDto;
import ai.ecma.apporderservice.payload.bonus.SaleResDto;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class SaleBonusServiceImpl implements SaleBonusService {
    private final SaleBonusRepository saleBonusRepository;
    private final ProductFeign productFeign;
    private final SaleBonusMapper saleBonusMapper;
    private final MessageByLang messageByLang;



    @SneakyThrows
    @Override
    public ApiResult<SaleBonusResDto> add(SaleBonusReqDto saleBonusReqDto) {

        //bu metod kiritilgan sana va vaqtlarni to'g'ri kiritilganligini tekshirib beradi
//        CommonUtils.validateTimeAndDateOrThrow(saleBonusReqDto.getStartDate(), saleBonusReqDto.getEndDate());

        //bu metod berilgan vaqt va sana oralig'idagi mavjud aksiyalarni tekshiradi bo'lsa klientga qaytaradi
        checkConflictTimeSaleBonus(
                saleBonusReqDto.getStartTime(),
                saleBonusReqDto.getEndTime(),
                saleBonusReqDto.getStartDate(),
                saleBonusReqDto.getEndDate(),
                null
        );

        SaleBonus saleBonus = saleBonusMapper.saleBonusReqDtoToSaleBonus(saleBonusReqDto);

        saleBonusRepository.save(saleBonus);
        SaleBonusResDto saleBonusResDto = saleBonusMapper.saleBonusResDto(saleBonus);

        return ApiResult.successResponse(saleBonusResDto);
    }

    @Override
    public ApiResult<SaleBonusViewDto> getOne(UUID id) {
        SaleBonus saleBonus = saleBonusRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("SALE_BONUS_NOT_FOUND")));

        //feign orqali product servicega borib saleid orqali saleni olib kelib qaytaramiz
        SaleResDto sale = productFeign.getOneSale(saleBonus.getSaleId()).getData();
        SaleBonusViewDto saleBonusViewDto = saleBonusMapper.saleBonusToSaleBonusViewDto(saleBonus,sale);

        return ApiResult.successResponse(saleBonusViewDto);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            saleBonusRepository.deleteById(id);
            return ApiResult.successResponse(messageByLang.getMessageByKey("DELETED"));
        } catch (Exception e) {
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("SALE_BONUS_NOT_FOUND"));
        }
    }

    @SneakyThrows
    @Override
    public ApiResult<SaleBonusResDto> edit(UUID id, SaleBonusReqDto saleBonusReqDto) {
        //bu metod kiritilgan sana va vaqtlarni to'g'ri kiritilganligini tekshirib beradi
//        CommonUtils.validateTimeAndDateOrThrow(
//                saleBonusReqDto.getStartDate(),
//                saleBonusReqDto.getEndDate());

        //bu metod berilgan vaqt va sana oralig'idagi mavjud aksiyalarni tekshiradi bo'lsa klientga qaytaradi

        Date currentDate = new Date(System.currentTimeMillis());
        java.sql.Date startDate = saleBonusReqDto.getStartDate();
        SaleBonus saleBonus = saleBonusRepository.findById(id).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("SALE_BONUS_NOT_FOUND")));
// Agar aksiya boshlangan bulsa faqat endDate fieldini uzgartirishimiz mumkin
        if (currentDate.after(startDate)) {
            saleBonus.setEndDate(saleBonusReqDto.getEndDate());
            checkConflictTimeSaleBonus(
                    saleBonus.getStartTime(),
                    saleBonus.getEndTime(),
                    saleBonus.getStartDate(),
                    saleBonus.getEndDate(),
                    id
            );

            SaleBonusResDto saleBonusResDto = saleBonusMapper.saleBonusResDto(saleBonus);
            saleBonusRepository.save(saleBonus);
            return ApiResult.successResponse(saleBonusResDto);

// Agar aksiya boshlanmagan bulsa hamma fieldlarini uzgartirishimiz mumkin
        } else {
            checkConflictTimeSaleBonus(
                    saleBonusReqDto.getStartTime(),
                    saleBonusReqDto.getEndTime(),
                    saleBonusReqDto.getStartDate(),
                    saleBonusReqDto.getEndDate(),
                    id
            );
            // Ushbu metod saleBonusReqDto dan olib saleBonus ga qiymatlarini qo'yib beradi
            saleBonusMapper.updateSaleBonus(saleBonusReqDto, saleBonus);
            saleBonusRepository.save(saleBonus);
            SaleBonusResDto saleBonusResDto = saleBonusMapper.saleBonusResDto(saleBonus);
            return ApiResult.successResponse(saleBonusResDto);
        }
    }

    /**
     * bu metod berilgan vaqt va sana oralig'idagi mavjud aksiyalarni tekshiradi bo'lsa klientga qaytaradi
     *
     * @param saleBonusReqDto ()
     */
    @Value("${kelishilganSana}")
    private String kelishilganSana;
    @Value("${kelishilganSanaPlusKun}")
    private String kelishilganSanaPlusKun;

    private void checkConflictTimeSaleBonus(Time startTime, Time endTime,
                                            java.sql.Date startDate, java.sql.Date endDate, UUID id) throws ParseException {


        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Date kelishilganSanaDate = dateFormat.parse(kelishilganSana);
        Date kelishilganSanaPlusBirKunDate = dateFormat.parse(kelishilganSanaPlusKun);
        long startTimeLong = kelishilganSanaDate.getTime() + startTime.getTime();
        long endTimeLong = kelishilganSanaPlusBirKunDate.getTime() + endTime.getTime();

        Timestamp startTimestamp = new Timestamp(startTimeLong);
        Timestamp endTimestamp = new Timestamp(endTimeLong);
        List<SaleBonus> existsByGivenTime;
        if (id != null)
            existsByGivenTime = saleBonusRepository.getExistsByGivenTimeIdNot(
                    startDate,
                    endDate,
                    startTimestamp,
                    endTimestamp,
                    kelishilganSana,
                    kelishilganSanaPlusKun,
                    id
            );
        else
            existsByGivenTime = saleBonusRepository.getExistsByGivenTime(
                    startDate,
                    endDate,
                    startTimestamp,
                    endTimestamp,
                    kelishilganSana,
                    kelishilganSanaPlusKun
            );

        if (!existsByGivenTime.isEmpty())
            throw new RestException(HttpStatus.CONFLICT, messageByLang.getMessageByKey("CONFLICT_TIME_AKSIYA"), existsByGivenTime);
    }




}
