package ai.ecma.apporderservice.service;

import ai.ecma.appdblib.entity.enums.OrderStatusEnum;
import ai.ecma.apporderservice.payload.*;

import java.util.List;
import java.util.UUID;

public interface OrderService {
    ApiResult<OrderBasketDto> addOrRemoveProduct(AddOrRemoveProductDto addOrRemoveProductDto);

    ApiResult<?> clearBasket();

    ApiResult<?> updateOrderProductPrice(OrderUpdatePriceDto orderUpdatePriceDto);

    ApiResult<?> addBoxNameToOrder(String boxName);

    ApiResult<?> orderConfirm(OrderConfirmDto orderConfirmDto);

    ApiResult<?> orderEditByOperator(OrderStatusEnum status, UUID orderId);

    ApiResult<CustomPage<OrderForOperatorDto>> getAllForOperator(int page, int size);

    ApiResult<?> cancelOrderByCustomer(UUID orderId);


    ApiResult<?> rejectOrderByKitchen(UUID orderId);

    ApiResult<?> readyOrderByKitchen(UUID orderId);

    ApiResult<?> viewOrdersByKitchen(UUID branchId);

    ApiResult<List<BranchInfoForClientDto>> orderSelectBranch(FindNearestBranchDto findNearestBranchDto);

    ApiResult<List<OrderForDispatcherDto>> getOrdersByBranchId(UUID branchId);

    ApiResult<String> addCourierToOrder(AddCourierToOrderDto addCourierToOrderDto);

    ApiResult<CourierOrderBoxDto> getOrderForCourier();

    ApiResult<?> courierChangeOrderStatusToClosed(UUID courierOrderId);

    ApiResult<CustomPage<ViewOrdersForClientDto>> viewOrdersForClient(int page, int size);

    ApiResult<String> addOrderAndDeliveryRate(AddOrderDeliveryRate addOrderDeliveryRate);

    ApiResult<FullDataOrderForClientDto> getFullDataOrderForClient(UUID orderId);

    ApiResult<List<CourierOrderBoxDto>> getHistoryOrderListForCourier();

    ApiResult<String> editOrderStatus(UUID orderId);
}
