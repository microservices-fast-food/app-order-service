package ai.ecma.apporderservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddBoxNameToOrderDto {
    @NotBlank(message = "Quti nomi bo'sh bo'lmasligi kerak")
    private String boxName;

    @NotNull(message = "productPriceBonusId bo'sh bo'lmasligi kerak")
    private UUID productPriceBonusId;
}
