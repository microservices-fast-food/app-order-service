package ai.ecma.apporderservice.controller;

import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.CustomPage;
import ai.ecma.apporderservice.payload.bonus.ProductCountBonusAddDto;
import ai.ecma.apporderservice.payload.bonus.ProductCountBonusDisplayDto;
import ai.ecma.apporderservice.payload.bonus.ProductCountBonusForBasketDto;
import ai.ecma.apporderservice.payload.bonus.ProductCountBonusInfoDto;
import ai.ecma.apporderservice.service.ProductCountBonusService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@Api(tags = "Product Count Bonus", description = "Mahsulot songa qarab bonus berish")
public class ProductCountBonusControllerImpl implements ProductCountBonusController{
   private final ProductCountBonusService productCountBonusService;

    public ProductCountBonusControllerImpl(ProductCountBonusService productCountBonusService) {
        this.productCountBonusService = productCountBonusService;
    }

    @ApiOperation(value = "Yangi Mahsulot soniga Bonus qo`shish")
    @Override
    public ApiResult<ProductCountBonusDisplayDto> addProductCountBonus(
            ProductCountBonusAddDto productCountBonusAddDto) {
        return productCountBonusService.addProductCountBonus(productCountBonusAddDto);
    }


    @Override
    public ApiResult<ProductCountBonusForBasketDto> getProductCountBonusForBasket(UUID id) {
        return productCountBonusService.getProductCountBonusForBasket(id);
    }

    @Override
    public ApiResult<CustomPage<ProductCountBonusInfoDto>> getAllProductCountBonusesList(
            int page, int size) {
        return productCountBonusService.getAllProductCountBonusesList(page, size);
    }

    @Override
    public ApiResult<ProductCountBonusInfoDto> editProductCountBonus(
            ProductCountBonusAddDto productCountBonusAddDto,
            UUID id) {
        return productCountBonusService.editProductCountBonus(
                productCountBonusAddDto, id
        );
    }

    @Override
    public ApiResult<?> deleteProductCountBonus(UUID id) {
        return productCountBonusService.deleteProductCountBonus(id);
    }
}
