package ai.ecma.apporderservice.mapper;


import ai.ecma.appdblib.entity.order.bonus.ProductPriceBonus;
import ai.ecma.apporderservice.payload.bonus.ProductPriceBonusBasketDto;
import ai.ecma.apporderservice.payload.bonus.ProductPriceBonusForAdminDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductPriceBonusMapper {

    ProductPriceBonusBasketDto productPriceBonusToBasketDto(ProductPriceBonus productPriceBonus);

    ProductPriceBonusForAdminDto productPriceBonusToForAdminDto(ProductPriceBonus productPriceBonus);
/**
Agar bizda natida classda fiel ko'p bo'lsa va 2 ta kiruvchidan olish kerak bo'lsa
mapperga @Context anotatsiyasi bilan yana 1 ta dto yani 2 ta kiruvchi parametr qilib 2 tasidan yigib olishimiz mumkin
**/
}