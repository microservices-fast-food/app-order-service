package ai.ecma.apporderservice.mapper;

import ai.ecma.appdblib.entity.order.bonus.DeliveryFreeBonus;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusAddDto;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusBasketDto;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusGetForAdmin;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeEditDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DeliveryFreeBonusMapper {
    // Ushbu metod DeliveryFreeBonus Listi dan DeliveryFreeBonusBasketDto Listi ni yasab beradi
    List<DeliveryFreeBonusBasketDto> deliveryFreeBonusToResDtoList(List<DeliveryFreeBonus> deliveryFreeBonusList);

    // Ushbu metod DeliveryFreeBonus Listi dan DeliveryFreeBonusForAdmin Listi ni yasab beradi
    List<DeliveryFreeBonusGetForAdmin> deliveryFreeBonusToResForAdminDto(List<DeliveryFreeBonus> deliveryFreeBonusList);

    // Ushbu metod DeliveryFreeBonus dan DeliveryFreeBonusBasketDto ni yasab beradi
    DeliveryFreeBonusBasketDto deliveryFreeBonusToResDto(DeliveryFreeBonus deliveryFreeBonus);

    // Ushbu metod deliveryFreeBonusReqDto dan olib deliveryFreeBonus ga qiymatlarini qo'yib beradi
    void updateDeliveryFreeBonus(DeliveryFreeEditDto deliveryFreeEditDto, @MappingTarget DeliveryFreeBonus deliveryFreeBonus);

    DeliveryFreeBonus deliveryFreeBonusTo(DeliveryFreeBonusAddDto deliveryFreeBonusAddDto);
}
