package ai.ecma.apporderservice.component;

import ai.ecma.appdblib.entity.enums.OrderStatusEnum;
import ai.ecma.appdblib.entity.order.Property;
import ai.ecma.appdblib.repository.order.OrderRepository;
import ai.ecma.appdblib.repository.order.PropertyRepository;
import ai.ecma.apporderservice.exception.RestException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;


@EnableScheduling
@Component
@RequiredArgsConstructor
public class ScheduleComponent {

    private final PropertyRepository propertyRepository;
    private final OrderRepository orderRepository;


    //Ushbu method orqali har daqiqada statusi Accepted bulgan, xozirgi va planReceivedTime vaqtlar oraligida bulgan order lar status i KITCHEN ga uzgartiriladi
    @SneakyThrows
    @Scheduled(fixedRate = 60000L)
    private void changeOrderStatus() {

        //Belgilangan vaqtni olish
        Property cookingNotificationTime = propertyRepository.findByKey("cookingNotificationTime").orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, "cookingNotificationTime"));
        long previousTime = 60000 * Long.parseLong(cookingNotificationTime.getValue());

        Date date = Date.valueOf(LocalDate.now());
        Timestamp previousTimestamp = new Timestamp(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(date + " " + LocalTime.now()).getTime()+ previousTime);
        Timestamp currentTimestamp = new Timestamp(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(date + " " + LocalTime.now()).getTime());

        //Accepted bulgan, xozirgi va planReceivedTime vaqtlar oraligida bulgan order lar status i KITCHEN ga uzgartirildi
        orderRepository.changeOrdersStatusForKitchen(OrderStatusEnum.ACCEPTED.name(),OrderStatusEnum.KITCHEN.name(), previousTimestamp,currentTimestamp);


    }


}
