package ai.ecma.apporderservice.payload.bonus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaleBonusBasketDto {
    private UUID id;
    private String name;
    private SaleBasketDto sale;
}
