package ai.ecma.apporderservice.controller;

import ai.ecma.appdblib.entity.enums.PayTypeEnum;
import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.PayTypeResDto;
import ai.ecma.apporderservice.utils.RestConstants;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstants.PAY_TYPE_CONTROLLER)
public interface PayTypeController {
    @GetMapping
    ApiResult<List<PayTypeResDto>> getAll();

    @GetMapping("/filter/{type}")
    ApiResult<List<PayTypeResDto>> getFilter(@PathVariable PayTypeEnum type);

    @GetMapping("/{id}")
    ApiResult<PayTypeResDto> getOne(@PathVariable UUID id);



}
