package ai.ecma.apporderservice.payload;

import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusBasketDto;
import ai.ecma.apporderservice.payload.bonus.ProductCountBonusForBasketDto;
import ai.ecma.apporderservice.payload.bonus.ProductPriceBonusBasketDto;
import ai.ecma.apporderservice.payload.bonus.SaleBonusBasketDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderBonusBasketDto {
    private UUID id;
    private ProductPriceBonusBasketDto productPriceBonus;
    private ProductCountBonusForBasketDto productCountBonus;
    private DeliveryFreeBonusBasketDto deliveryFreeBonus;
    private SaleBonusBasketDto saleBonus;
    private String boxName;
}
