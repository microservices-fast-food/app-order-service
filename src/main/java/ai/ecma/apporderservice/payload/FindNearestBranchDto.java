package ai.ecma.apporderservice.payload;

import ai.ecma.appdblib.entity.enums.OrderTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor

//clientga eng yaqin branch larni topish uchun product service ga beriladigan class
public class FindNearestBranchDto {
    private OrderTypeEnum type;
    private UUID addressId;
    private Set<UUID> productIds;
    private Time planReceivedTime;
}
