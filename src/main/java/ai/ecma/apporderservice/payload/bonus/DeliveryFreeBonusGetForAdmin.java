package ai.ecma.apporderservice.payload.bonus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.sql.Time;

@AllArgsConstructor
@NoArgsConstructor
@Data
// Adminga ko'rinishi kerak bo'lgan fieldlar
public class DeliveryFreeBonusGetForAdmin {

    private String name;

    private Time startTime;

    private Time endTime;

    private Date startDate;

    private Date endDate;

    private Boolean active;

    private Double minPrice;
}
