package ai.ecma.apporderservice.payload.bonus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.sql.Time;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductCountBonusInfoDto {
    private UUID id;
    private String name;
    private Time startTime;
    private Time endTime;
    private Date startDate;
    private Date endDate;
    private String productName; //
    private Integer productCount;
    private String giftProductName;//
    private Integer giftProductCount;
    private String photoUrl; //
}
