package ai.ecma.apporderservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CourierOrderBoxDto {
    private UUID courierId;
    private List<OrderForCourierDto> orderForCourierDtoList;
}
