package ai.ecma.apporderservice.controller;

import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.bonus.SaleBonusReqDto;
import ai.ecma.apporderservice.payload.bonus.SaleBonusResDto;
import ai.ecma.apporderservice.payload.bonus.SaleBonusViewDto;
import ai.ecma.apporderservice.service.SaleBonusService;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class SaleBonusControllerImpl implements SaleBonusController{
    private final SaleBonusService saleBonusService;

    public SaleBonusControllerImpl(SaleBonusService saleBonusService) {
        this.saleBonusService = saleBonusService;
    }

    @Override
    public ApiResult<SaleBonusResDto> add(SaleBonusReqDto saleBonusReqDto) {
        return saleBonusService.add(saleBonusReqDto);
    }

    @Override
    public ApiResult<SaleBonusViewDto> getOne(UUID id) {
        return saleBonusService.getOne(id);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        return saleBonusService.delete(id);
    }

    @Override
    public ApiResult<SaleBonusResDto> edit(UUID id, SaleBonusReqDto saleBonusReqDto) {
        return saleBonusService.edit(id,saleBonusReqDto);
    }
}
