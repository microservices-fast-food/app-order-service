package ai.ecma.apporderservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentResDto {

    private UUID id;

    private String title;

    private String text;

    private Boolean complaint;

    private UUID attachmentId;

    private String photoUrl;

    private UUID orderId;

    private UUID userId;

    private Boolean unread;
}
