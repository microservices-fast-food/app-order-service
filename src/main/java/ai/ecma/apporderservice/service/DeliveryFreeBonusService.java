package ai.ecma.apporderservice.service;

import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.CustomPage;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusAddDto;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusBasketDto;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusGetForAdmin;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeEditDto;

import java.util.UUID;

public interface DeliveryFreeBonusService {
    ApiResult<DeliveryFreeBonusBasketDto> add(DeliveryFreeBonusAddDto deliveryFreeBonusAddDto);

    ApiResult<CustomPage<DeliveryFreeBonusBasketDto>> getAll(int page, int size);

    ApiResult<CustomPage<DeliveryFreeBonusGetForAdmin>> getForAdmin(int page, int size);

    ApiResult<DeliveryFreeBonusBasketDto> getOne(UUID id);

    ApiResult<DeliveryFreeBonusBasketDto> edit(DeliveryFreeEditDto deliveryFreeEditDto, UUID id);

    ApiResult<?> delete(UUID id);
}
