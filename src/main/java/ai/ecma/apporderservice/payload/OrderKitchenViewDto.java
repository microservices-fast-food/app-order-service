package ai.ecma.apporderservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderKitchenViewDto {
    private UUID orderId;
    private Timestamp planReceivedTime;
    private Timestamp createdAt;
    private List<ProductKitchenViewDto> products;
    private Long code;
}
