package ai.ecma.apporderservice.service;

import ai.ecma.appdblib.entity.enums.BonusTypeEnum;
import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.order.bonus.BoxProduct;
import ai.ecma.appdblib.entity.order.bonus.ProductPriceBonus;
import ai.ecma.appdblib.repository.order.BoxProductRepository;
import ai.ecma.appdblib.repository.order.ProductPriceBonusRepository;
import ai.ecma.apporderservice.aop.annotation.CheckAuth;
import ai.ecma.apporderservice.component.MessageByLang;
import ai.ecma.apporderservice.exception.RestException;
import ai.ecma.apporderservice.feign.ProductFeign;
import ai.ecma.apporderservice.mapper.ProductMapper;
import ai.ecma.apporderservice.mapper.ProductPriceBonusMapper;
import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.bonus.*;
import ai.ecma.apporderservice.utils.CommonUtils;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductPriceBonusServiceImpl implements ProductPriceBonusService {
    private final ProductPriceBonusRepository productPriceBonusRepository;
    private final ProductPriceBonusMapper productPriceBonusMapper;
    private final BoxProductRepository boxProductRepository;
    private final ProductMapper productMapper;
    private final ProductFeign productFeign;
    private final MessageByLang messageByLang;

    @Value("${kelishilganSana}")
    private String kelishilganSana;

    @Value("${kelishilganSanaPlusKun}")
    private String kelishilganSanaPlusKun;

    @Override
    public ApiResult<ProductPriceBonusBasketDto> getForBasket(UUID id) {
        ProductPriceBonus productPriceBonus = productPriceBonusRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_PRICE_BONUS_NOT_FOUND")));
        return ApiResult.successResponse(productPriceBonusMapper.productPriceBonusToBasketDto(productPriceBonus));
    }

    @Override
    public ApiResult<List<BoxProductForProductPriceBonusDto>> getBoxProductByProductPriceBonus(UUID productPriceBonusId) {
        List<BoxProduct> boxProducts = boxProductRepository.findAllByProductPriceBonusId(productPriceBonusId);
        if (boxProducts.isEmpty())
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("BOX_NAME_NOT_FOUNT"));

        //boxproductlarni ichidan box nomlari va nechta producti borligini qaytaradi
        List<BoxProductForProductPriceBonusDto> byBoxNameAndCountProduct = getByBoxNameAndCountProduct(boxProducts);
        return ApiResult.successResponse(byBoxNameAndCountProduct);
    }

    @Override
    public ApiResult<List<ProductForBoxProductDto>> getProductForBoxProductClient(UUID productPriceBonusId, String name) {
        Set<String> productIdsString = boxProductRepository.getAllProductIdByBoxNameAndProductPriceBonusId(productPriceBonusId, name);
        Set<UUID> productIds = productIdsString.stream().map(UUID::fromString).collect(Collectors.toSet());

        List<ProductResDto> products = productFeign.getProductsByIds(productIds).getData();
        return ApiResult.successResponse(productMapper.productResToProductForBoxProductDtoList(products));
    }

    @CheckAuth(permission = PermissionEnum.ADD_BONUS)
    @SneakyThrows
    @Transactional
    @Override
    public ApiResult<ProductPriceBonusGetSelectDto> add(ProductPriceBonusAddDto productPriceBonusAddDto) {
        //bu metod kiritilgan sanani to'g'ri kiritilganligini tekshirib beradi
        CommonUtils.validateTimeAndDateOrThrow(productPriceBonusAddDto.getStartDate(),
                productPriceBonusAddDto.getEndDate(),
                productPriceBonusAddDto.getStartTime(),
                productPriceBonusAddDto.getEndTime());

        //bu metod berilgan vaqt va sana oralig'idagi mavjud aksiyalarni tekshiradi bo'lsa klientga qaytaradi
        checkConflictTimeProductPriceBonus(productPriceBonusAddDto.getStartDate(),
                productPriceBonusAddDto.getEndDate(),
                productPriceBonusAddDto.getStartTime(),
                productPriceBonusAddDto.getEndTime(),
                null);

        ProductPriceBonus productPriceBonus = new ProductPriceBonus(
                productPriceBonusAddDto.getName(),
                BonusTypeEnum.PRODUCT_PRICE_BONUS,
                productPriceBonusAddDto.getStartTime(),
                productPriceBonusAddDto.getEndTime(),
                productPriceBonusAddDto.getStartDate(),
                productPriceBonusAddDto.getEndDate(),
                productPriceBonusAddDto.getMinPrice()
        );
        productPriceBonusRepository.save(productPriceBonus);

        //Mahsulot bonusga box qo'shadi va saqlaydi
        boxProductAddProductPriceBonus(productPriceBonus.getId(), productPriceBonusAddDto.getBoxProductAddDtos());

        //Qaytaruvchi klasni yaratdik
        ProductPriceBonusGetSelectDto productPriceBonusGetSelectDto = new ProductPriceBonusGetSelectDto(productPriceBonus.getId(),
                productPriceBonus.getName()
        );
        return ApiResult.successResponse(productPriceBonusGetSelectDto);
    }

    @CheckAuth(permission = PermissionEnum.EDIT_BONUS)
    @SneakyThrows
    @Override
    public ApiResult<ProductPriceBonusForAdminDto> edit(UUID id, ProductPriceBonusAddDto productPriceBonusAddDto) {

        //bu metod kiritilgan sanani to'g'ri kiritilganligini tekshirib beradi
        CommonUtils.validateTimeAndDateOrThrow(productPriceBonusAddDto.getStartDate(),
                productPriceBonusAddDto.getEndDate(),
                productPriceBonusAddDto.getStartTime(),
                productPriceBonusAddDto.getEndTime());

        ProductPriceBonus productPriceBonus = productPriceBonusRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_PRICE_BONUS_NOT_FOUND")));

        //aksiya boshlanmaganligini tekshiramiz.Agar aksiya boshlanmagan bo'lsa hamma feild ni o'zgartira olamiz
        if (productPriceBonusAddDto.getStartDate().before(Date.valueOf(LocalDate.now()))) {


            //bu metod berilgan vaqt va sana oralig'idagi mavjud aksiyalarni tekshiradi bo'lsa klientga qaytaradi
            checkConflictTimeProductPriceBonus(productPriceBonusAddDto.getStartDate(),
                    productPriceBonusAddDto.getEndDate(),
                    productPriceBonusAddDto.getStartTime(),
                    productPriceBonusAddDto.getEndTime(),
                    id);

            productPriceBonus.setMinPrice(productPriceBonusAddDto.getMinPrice());
            productPriceBonus.setName(productPriceBonusAddDto.getName());
            productPriceBonus.setStartDate(productPriceBonusAddDto.getStartDate());
            productPriceBonus.setEndDate(productPriceBonusAddDto.getEndDate());
            productPriceBonus.setStartTime(productPriceBonusAddDto.getStartTime());
            productPriceBonus.setEndTime(productPriceBonusAddDto.getEndTime());
        }
        //agarda aksiya boshlangan bo'lsa faqat tugash sanasini o'zgartiramiz
        else {
            productPriceBonus.setEndDate(productPriceBonusAddDto.getEndDate());
            checkConflictTimeProductPriceBonus(
                    productPriceBonus.getStartDate(),
                    productPriceBonus.getEndDate(),
                    productPriceBonus.getStartTime(),
                    productPriceBonus.getEndTime(),
                    id);
        }
        productPriceBonusRepository.save(productPriceBonus);
        return ApiResult.successResponse(productPriceBonusMapper.productPriceBonusToForAdminDto(productPriceBonus));
    }

    @CheckAuth(permission = PermissionEnum.DELETE_BONUS)
    @Transactional
    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            boxProductRepository.deleteAllByProductPriceBonusId(id);
            productPriceBonusRepository.deleteById(id);
            return ApiResult.successResponse(messageByLang.getMessageByKey("DELETED"));
        } catch (Exception e) {
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_PRICE_BONUS_NOT_FOUND"));
        }
    }

    //bu metod berilgan vaqt va sana oralig'idagi mavjud aksiyalarni tekshiradi bo'lsa klientga qaytaradi
    public void checkConflictTimeProductPriceBonus(Date startDate, Date endDate,
                                                   Time startTime, Time endTime,
                                                   UUID id) throws ParseException {
        Timestamp startTimeNew = new Timestamp(new SimpleDateFormat("yyy-MM-dd hh:mm:ss").parse(kelishilganSana + " " + startTime).getTime());
        Timestamp endTimeNew = new Timestamp(new SimpleDateFormat("yyy-MM-dd hh:mm:ss").parse(kelishilganSana + " " + endTime).getTime());

        if (startTime.before(endTime)) {
            endTimeNew = new Timestamp(new SimpleDateFormat("yyy-MM-dd hh:mm:ss").parse(kelishilganSanaPlusKun + " " + endTime).getTime());
        }

        List<ProductPriceBonus> existingProductPriceBonuses;
        if (id != null) {
            existingProductPriceBonuses = productPriceBonusRepository.getExistsByGivenTimeIdNot(
                    startDate,
                    endDate,
                    startTimeNew,
                    endTimeNew,
                    kelishilganSana,
                    kelishilganSanaPlusKun,
                    id
            );
        } else {
            Date dateKelishilgan =Date.valueOf(kelishilganSana);
            Date dateKelishilganPlusKUn = Date.valueOf(kelishilganSanaPlusKun);
            existingProductPriceBonuses = productPriceBonusRepository.getExistsByGivenTime(
                    startDate,
                    endDate,
                    startTimeNew,
                    endTimeNew,
                    dateKelishilgan,
                    dateKelishilganPlusKUn
            );
        }

        if (!existingProductPriceBonuses.isEmpty())
            throw new RestException(HttpStatus.CONFLICT, messageByLang.getMessageByKey("CONFLICT_TIME_AKSIYA"),
                    existingProductPriceBonuses.stream().map(productPriceBonusMapper::productPriceBonusToForAdminDto).collect(Collectors.toList()));

    }


    //Mahsulot bonusga box qo'shadi va saqlaydi
    private void boxProductAddProductPriceBonus(UUID productPriceBonusId, List<BoxProductAddDto> boxProductAddDtos) {
        List<BoxProduct> boxProductList = new ArrayList<>();
        for (BoxProductAddDto boxProductAddDto : boxProductAddDtos) {
            BoxProduct boxProduct = new BoxProduct(
                    boxProductAddDto.getProductId(),
                    boxProductAddDto.getBoxName(),
                    productPriceBonusId
            );
            boxProductList.add(boxProduct);
        }

        boxProductRepository.saveAll(boxProductList);
    }

    //boxproduct listdan boxlani nomi va nechta producti borligini qaytaradi
    private List<BoxProductForProductPriceBonusDto> getByBoxNameAndCountProduct(List<BoxProduct> boxProducts) {
        Set<String> boxNames = new HashSet<>();
        for (BoxProduct boxProduct : boxProducts) {
            boxNames.add(boxProduct.getBoxName());
        }

        List<BoxProductForProductPriceBonusDto> listBoxProduct = new ArrayList<>();
        for (String boxName : boxNames) {
            int count = 0;
            for (BoxProduct boxProduct : boxProducts) {
                if (boxName.equals(boxProduct.getBoxName()))
                    count++;
            }
            BoxProductForProductPriceBonusDto boxProductForProductPriceBonusDto = new BoxProductForProductPriceBonusDto(
                    boxName,
                    count
            );
            listBoxProduct.add(boxProductForProductPriceBonusDto);
        }
        return listBoxProduct;
    }
}
