package ai.ecma.apporderservice.component;

import ai.ecma.appdblib.entity.enums.PayTypeEnum;
import ai.ecma.appdblib.entity.order.PayType;
import ai.ecma.appdblib.entity.order.Property;
import ai.ecma.appdblib.repository.order.PayTypeRepository;
import ai.ecma.appdblib.repository.order.PropertyRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class DataLoader implements CommandLineRunner {
    private final PayTypeRepository payTypeRepository;
    private final PropertyRepository propertyRepository;

    public DataLoader(PayTypeRepository payTypeRepository, PropertyRepository propertyRepository) {
        this.payTypeRepository = payTypeRepository;
        this.propertyRepository = propertyRepository;
    }

    @Value("${spring.sql.init.mode}")
    public String dataLoaderMode;

    @Override
    public void run(String... args) throws Exception {

        if (dataLoaderMode.equals("always")) {
            List<PayType> payTypeList = new ArrayList<>();
            payTypeList.add(
                    new PayType(
                            "PAYME",
                            PayTypeEnum.CARD
                    ));
            payTypeList.add(
                    new PayType(
                            "CLICK",
                            PayTypeEnum.CARD
                    ));
            payTypeList.add(
                    new PayType(
                            "CASH",
                            PayTypeEnum.CASH
                    ));
            payTypeRepository.saveAll(payTypeList);

            List<Property> propertyList = new ArrayList<>(Arrays.asList(
                    new Property(
                            "reliabilityRate",
                            "20"
                    ),
                    new Property(
                            "cookingNotificationTime",
                            "20"
                    )
            ));

            propertyRepository.saveAll(propertyList);
        }

    }
}
