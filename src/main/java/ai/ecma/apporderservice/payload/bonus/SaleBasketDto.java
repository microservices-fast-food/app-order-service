package ai.ecma.apporderservice.payload.bonus;

import ai.ecma.appdblib.entity.enums.SaleTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaleBasketDto {
    private UUID id;
    private Double amount;
    private SaleTypeEnum type;
}
