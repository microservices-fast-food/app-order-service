package ai.ecma.apporderservice.payload;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentReqDto {

    @NotBlank(message = "Sarlavha bo'sh bo'lmasligi kerak")
    private String title;

    @NotBlank(message = "Matn bo'sh bo'lmasligi kerak")
    private String text;

    @NotBlank(message = "Shikoyat bo'sh bo'lmasligi kerak")
    private Boolean complaint;

    private UUID attachmentId;

    private UUID orderId;

}
