package ai.ecma.apporderservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddCourierToOrderDto {
    @NotNull(message = "{COURIER_ID_NOT_NULL}")
    private UUID courierId;

    @NotNull(message = "orderId bo'sh bolmasligi kerak")
    private UUID orderId;

    @NotNull(message = "courierOrder yopilgan yoki yopilgan yopilmaganligini tanlash kerak")
    private boolean close;

}
