package ai.ecma.apporderservice.config;

import feign.RequestInterceptor;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Muhammad Mo'minov
 * 23.09.2021
 */
@Configuration
public class FeignConfig {
    @Value("${service.orderServiceUsername}")
    private String orderServiceUsername;
    @Value("${service.orderServicePassword}")
    private String orderServicePassword;

    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            requestTemplate.header("serviceName", orderServiceUsername);
            requestTemplate.header("servicePassword", orderServicePassword);
            requestTemplate.header("Accept", ContentType.APPLICATION_JSON.getMimeType());
        };
    }
}
