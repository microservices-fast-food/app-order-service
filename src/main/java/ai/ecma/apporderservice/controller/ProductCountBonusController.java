package ai.ecma.apporderservice.controller;

import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.CustomPage;
import ai.ecma.apporderservice.payload.bonus.ProductCountBonusAddDto;
import ai.ecma.apporderservice.payload.bonus.ProductCountBonusDisplayDto;
import ai.ecma.apporderservice.payload.bonus.ProductCountBonusForBasketDto;
import ai.ecma.apporderservice.payload.bonus.ProductCountBonusInfoDto;
import ai.ecma.apporderservice.utils.RestConstants;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

import static ai.ecma.apporderservice.utils.AppConstant.DEFAULT_PAGE_NUMBER;
import static ai.ecma.apporderservice.utils.AppConstant.DEFAULT_PAGE_SIZE;

@RequestMapping(RestConstants.PRODUCT_COUNT_BONUS_CONTROLLER)
public interface ProductCountBonusController {

    /**
     * 1. Admin tomonidan ProductCountBonus
     * (buyurtmadagi mahsulot soniga bog'liq aksiya) qo'shish -
     * biror mahsulotdan nechtadir sotib olinganda
     * yana nechtadir qaysidir mahsulot qo’shilishi
     *
     * @param productCountBonusAddDto ({
     *                                name: String,
     *                                startTime: Timestamp,
     *                                endTime: Timestamp,
     *                                startDate: Date,
     *                                endDate: Date,
     *                                active: Boolean,
     *                                productId: UUID,
     *                                productCount: Integer,
     *                                giftProductId: UUID,
     *                                giftProductCount: Integer})
     * @return productCountBonusDisplayDto
     * ({id: UUID, name : String})
     */
    @PostMapping
    ApiResult<ProductCountBonusDisplayDto> addProductCountBonus(
            @RequestBody @Valid ProductCountBonusAddDto productCountBonusAddDto);

    /**
     * 2. ProductCountBonus
     * (buyurtmadagi mahsulot soniga bog'liq aksiya) ni Savatchada ko'rsatish
     * uchun qaytaradi
     *
     * @param id (id: UUID) - ko'rsatilishi kerak bo'lgan aksiyaning id si
     * @return productCountBonusForBasketDto
     * ({giftProductName: String,
     * giftProductCount: Integer
     * photoUrl: String})
     * (so'rov berilgan aksiyaga tegishli
     * bonus mahsulotning nomi, soni va rasmini yuklash uchun url qaytarish)
     */
    @GetMapping("/basket/{id}")
    ApiResult<ProductCountBonusForBasketDto> getProductCountBonusForBasket(
            @PathVariable UUID id);


    /**
     * 3. Barcha ProductCountBonus
     * (buyurtmadagi mahsulot soniga bog'liq aksiya) larning
     * ro'yxatini page ko'rinishida qaytaradi
     *
     * @param page so'ralgan sahifa raqami
     * @param size bitta sahifadagi elementlar (productCountBonus lar) soni
     * @return CustomPage<ProductCountBonusDisplayDto>
     * so'ralgan miqdordagi productCountBonus larning id va name field lari
     */
    @GetMapping("/list")
    ApiResult<CustomPage<ProductCountBonusInfoDto>> getAllProductCountBonusesList(
            @RequestParam(defaultValue = DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) int size);


    /**
     * 4. ProductCountBonus
     * (buyurtmadagi mahsulot soniga bog'liq aksiya) ning
     * field lariga o'zgartirish kiritish
     * 4.1 Holati active
     * (boshlanish vaqti hozirgi vaqtdan oldin va tugash vaqti hozirdan keyin bo'lgan)
     * ProductCountBonus
     * (buyurtmadagi mahsulot soniga bog'liq aksiya) ning endDate (aksiyaning tugash sanasi)
     * field ini o'zgartirish
     * 4.2 ProductCountBonus
     *  (buyurtmadagi mahsulot soniga bog'liq aksiya) ning
     *  barcha field lariga o'zgartirish kiritish
     * @param productCountBonusAddDto ({
     *                                  name: String,
     *                                  startTime: Timestamp,
     *                                  endTime: Timestamp,
     *                                  startDate: Date,
     *                                  endDate: Date,
     *                                  active: Boolean,
     *                                  productId: UUID,
     *                                  productCount: Integer,
     *                                  giftProductId: UUID,
     *                                  giftProductCount: Integer})
     * @param id (id: UUID) - o'zgartirilishi kerak bo'lgan aksiyaning id si
     * @return productCountBonusDisplayDto
     * ({id: UUID, name : String})
     */
    @PutMapping("/edit/{id}")
    ApiResult<ProductCountBonusInfoDto> editProductCountBonus(
            @RequestBody @Valid ProductCountBonusAddDto productCountBonusAddDto,
            @PathVariable UUID id);

    /**
     * 6. ProductCountBonus
     * (buyurtmadagi mahsulot soniga bog'liq aksiya) ni o'chirish
     *
     * @param id (id: UUID) - o'chirilishi kerak bo'lgan aksiyaning id si
     * @return Aksiya o'chirilganligi haqida xabar
     */
    @DeleteMapping("/delete/{id}")
    ApiResult<?> deleteProductCountBonus(@PathVariable UUID id);
}
