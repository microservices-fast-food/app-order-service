package ai.ecma.apporderservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
public class RequestFilter implements Filter {
    @Value("${serviceUsername}")
    private String serviceUsernameKey;
    @Value("${servicePassword}")
    private String servicePasswordKey;

    @Value("${service.productServiceUsername}")
    private String productServiceUsername;
    @Value("${service.productServicePassword}")
    private String productServicePassword;

    @Value("${service.authServiceUsername}")
    private String authServiceUsername;
    @Value("${service.authServicePassword}")
    private String authServicePassword;

    @Value("${service.gatewayServiceUsername}")
    private String gatewayServiceUsername;
    @Value("${service.gatewayServicePassword}")
    private String gatewayServicePassword;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest =(HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        String serviceUsername = httpServletRequest.getHeader(serviceUsernameKey);
        String servicePassword = httpServletRequest.getHeader(servicePasswordKey);
        String requestURI = httpServletRequest.getRequestURI(); //v2/api-docs
        if(!requestURI.equals("/v2/api-docs")) {
            if (!((productServiceUsername.equals(serviceUsername) && productServicePassword.equals(servicePassword)) ||
                    (authServiceUsername.equals(serviceUsername) && authServicePassword.equals(servicePassword)) ||
                    (gatewayServiceUsername.equals(serviceUsername) && gatewayServicePassword.equals(servicePassword))
            )) {
//                throw new RestException(HttpStatus.FORBIDDEN, "FORBIDDEN");
                httpServletResponse.setStatus(401);
                httpServletResponse.setContentType("application/json");
            }
        }

        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }


}
