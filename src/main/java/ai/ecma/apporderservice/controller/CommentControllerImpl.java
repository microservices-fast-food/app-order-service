package ai.ecma.apporderservice.controller;

import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.CommentReqDto;
import ai.ecma.apporderservice.payload.CommentResDto;
import ai.ecma.apporderservice.payload.CustomPage;
import ai.ecma.apporderservice.service.CommentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@Slf4j
public class CommentControllerImpl implements CommentController {

    private final CommentService commentService;

    @Override
    public ApiResult<List<CommentResDto>> getAllByOrderId(UUID id) {
        log.info("CommentController getAllByOrderId req id:{}", id);
        ApiResult<List<CommentResDto>> apiResult = commentService.getAllByOrderId(id);
        log.info("CommentController getAllByOrderId resp : success");
        return apiResult;
    }

    @Override
    public ApiResult<CustomPage<CommentResDto>> getAll(int page, int size) {
        log.info("CommentController getAll req page:{}, size:{}", page, size);
        ApiResult<CustomPage<CommentResDto>> apiResult = commentService.getAll(page, size);
        log.info("CommentController getAll resp : success");
        return apiResult;
    }

    @Override
    public ApiResult<CustomPage<CommentResDto>> getAllRead(int page, int size) {
        log.info("CommentController getAll req page:{}, size:{}", page, size);
        ApiResult<CustomPage<CommentResDto>> apiResult = commentService.getAllRead(page, size);
        log.info("CommentController getAll resp : success");
        return apiResult;
    }

    @Override
    public ApiResult<CustomPage<CommentResDto>> getAllUnread(int page, int size) {
        log.info("CommentController getAll req page:{}, size:{}", page, size);
        ApiResult<CustomPage<CommentResDto>> apiResult = commentService.getAllUnread(page, size);
        log.info("CommentController getAll resp : success");
        return apiResult;
    }

    @Override
    public ApiResult<CommentResDto> getComment(UUID id) {
        log.info("CommentController getComment req id:{}", id);
        ApiResult<CommentResDto> apiResult = commentService.getComment(id);
        log.info("CommentController getComment resp ApiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<CommentResDto> add(CommentReqDto commentReqDto) {
        log.info("CommentController add req CommentReqDto : {}", commentReqDto);
        ApiResult<CommentResDto> apiResult = commentService.addComment(commentReqDto);
        log.info("CommentController add resp ApiResult :{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<CommentResDto> readComment(UUID id) {
        log.info("CommentController edit req id :{}", id);
        ApiResult<CommentResDto> apiResult = commentService.readComment(id);
        log.info("CommentController edit resp ApiResult :{}", apiResult);
        return apiResult;
    }

//    @Override
//    public ApiResult<?> delete(UUID id) {
//        log.info("CommentController delete req id :{}", id);
//        ApiResult<?> apiResult = commentService.deleteComment(id);
//        log.info("CommentController delete resp : success");
//        return apiResult;
//    }

}
