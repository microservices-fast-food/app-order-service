package ai.ecma.apporderservice.payload.bonus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DeliveryFreeBonusBasketDto {

    private UUID id;
    private String name;
}
