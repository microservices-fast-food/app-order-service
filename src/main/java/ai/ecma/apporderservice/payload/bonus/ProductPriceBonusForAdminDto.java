package ai.ecma.apporderservice.payload.bonus;

import ai.ecma.appdblib.entity.enums.BonusTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.sql.Time;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductPriceBonusForAdminDto {
    private String name;

    private BonusTypeEnum type;

    private Time startTime;

    private Time endTime;

    private Date startDate;

    private Date endDate;

    private Double minPrice;
}
