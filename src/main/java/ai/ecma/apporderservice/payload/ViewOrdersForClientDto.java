package ai.ecma.apporderservice.payload;

import ai.ecma.appdblib.entity.enums.OrderStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ViewOrdersForClientDto {
    private UUID id;
    private Long code;
    private Timestamp createdAt;
    private Double totalPrice;
    private OrderStatusEnum status;
}
