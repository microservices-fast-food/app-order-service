package ai.ecma.apporderservice.controller;

import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.bonus.*;
import ai.ecma.apporderservice.service.ProductPriceBonusService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class ProductPriceBonusControllerImpl implements ProductPriceBonusController{
    private final ProductPriceBonusService productPriceBonusService;

    public ProductPriceBonusControllerImpl(ProductPriceBonusService productPriceBonusService) {
        this.productPriceBonusService = productPriceBonusService;
    }

    @Override
    public ApiResult<ProductPriceBonusBasketDto> getForBasket(UUID id) {
        return productPriceBonusService.getForBasket(id);
    }

    @Override
    public ApiResult<List<BoxProductForProductPriceBonusDto>> getBoxProductByProductPriceBonus(UUID productPriceBonusId) {
        return productPriceBonusService.getBoxProductByProductPriceBonus(productPriceBonusId);
    }

    @Override
    public ApiResult<List<ProductForBoxProductDto>> getProductForBoxProductClient(UUID id, String name) {
        return productPriceBonusService.getProductForBoxProductClient(id, name);
    }


    @Override
    public ApiResult<ProductPriceBonusGetSelectDto> add(ProductPriceBonusAddDto productPriceBonusAddDto) {
        return productPriceBonusService.add(productPriceBonusAddDto);
    }

    @Override
    public ApiResult<ProductPriceBonusForAdminDto> edit(UUID id, ProductPriceBonusAddDto productPriceBonusAddDto) {
        return productPriceBonusService.edit(id, productPriceBonusAddDto);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        return productPriceBonusService.delete(id);
    }
}
