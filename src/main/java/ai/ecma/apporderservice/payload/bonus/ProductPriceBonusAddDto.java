package ai.ecma.apporderservice.payload.bonus;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductPriceBonusAddDto {
    @NotBlank(message = "Nomi bo'sh bo'lmasligi kerak")
    private String name;

    @ApiModelProperty(dataType = "string", example = "09:30:00")
    @NotNull(message = "Boshlanish vaqti bo'sh bo'lmasligi kerak")
    private Time startTime;

    @ApiModelProperty(dataType = "string", example = "09:30:00")
    @NotNull(message = "Tugash vaqti bo'sh bo'lmasligi kerak")
    private Time endTime;

//    @FutureOrPresent(message = "Berilgan vaqt hozirgi vaqtdan keyin bo'lishi kerak")
    @NotNull(message = "Boshlanish sanasi bo'sh bo'lmasligi kerak")
    private Date startDate;

//    @FutureOrPresent(message = "Berilgan vaqt hozirgi vaqtdan keyin bo'lishi kerak")
    @NotNull(message = "Tugash sanasi bo'sh bo'lmasligi kerak")
    private Date endDate;

    @Positive(message = "Narxi bo'sh bo'lmasligi kerak")
    private Double minPrice;

    @Valid
    @NotEmpty(message = "boxProductAdd bo'sh bo'lmasligi kerak")
    private List<BoxProductAddDto> boxProductAddDtos;

}
