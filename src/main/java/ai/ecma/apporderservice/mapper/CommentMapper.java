package ai.ecma.apporderservice.mapper;

import ai.ecma.appdblib.entity.order.Comment;
import ai.ecma.apporderservice.payload.CommentReqDto;
import ai.ecma.apporderservice.payload.CommentResDto;
import ai.ecma.apporderservice.utils.CommonUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring",imports = CommonUtils.class)
public interface CommentMapper {
//    Ushbu comment ichidagi photoUrl ni yasab berishi uchun expression yani shart yordamida commonUtilsga attachmentId berib yuborish orqali bajarildi
    @Mapping(target = "photoUrl", expression = "java(CommonUtils.buildPhotoUrl(comment.getAttachmentId()))")
    CommentResDto commentToResDto(Comment comment);

    List<CommentResDto> commentListToResDtoList(List<Comment> commentList);

    @Mapping(target = "userId",expression = "java(CommonUtils.getUserFromRequest().getId())")
    Comment commentReqDtoToComment(CommentReqDto commentReqDto);
}
