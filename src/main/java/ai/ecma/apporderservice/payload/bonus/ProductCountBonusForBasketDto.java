package ai.ecma.apporderservice.payload.bonus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductCountBonusForBasketDto {
    private UUID id;
    private String giftProductName;
    private Integer giftProductCount;
    private String photoUrl;
}
