package ai.ecma.apporderservice.mapper;

import ai.ecma.apporderservice.payload.ProductBasketDto;
import ai.ecma.apporderservice.payload.ProductForClientViewDto;
import ai.ecma.apporderservice.payload.bonus.ProductForBoxProductDto;
import ai.ecma.apporderservice.payload.bonus.ProductResDto;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    @Mapping(target = "count", expression = "java(getProductCount(productCountMap,productResDto.getId()))")
    ProductBasketDto productResDtoToBasketDto(ProductResDto productResDto,
                                              @Context HashMap<UUID, Integer> productCountMap);

    List<ProductBasketDto> productListToProductBasketDtoList(List<ProductResDto> productResDtos,
                                                             @Context HashMap<UUID, Integer> productCountMap);

    List<ProductForBoxProductDto> productResToProductForBoxProductDtoList(List<ProductResDto> productResDtos);





    @Mapping(target = "count",expression = "java(getProductCount(productCountMap,productResDto.getId()))")
    @Mapping(target = "gift",expression = "java(isGiftProduct(giftProductIds,productResDto.getId()))")
    ProductForClientViewDto productResDtoToProductForClientViewDto(ProductResDto productResDto,
                                                             @Context HashMap<UUID, Integer> productCountMap,
                                                             @Context Set<String> giftProductIds);

    List<ProductForClientViewDto> productResDtosToProductForClientViewDtoList(List<ProductResDto> productResDtos,
                                                                              @Context HashMap<UUID, Integer> productCountMap,
                                                                              @Context Set<String> giftProductIds);

    default boolean isGiftProduct(Set<String> ids, UUID productId){
        for (String id : ids) {
            if (id.equals(productId.toString()))
                return true;
        }
        return false;
    }

    default Integer getProductCount(HashMap<UUID, Integer> productCountMap, UUID productId){
        if (productCountMap.containsKey(productId)) {
            return productCountMap.get(productId);
        }
        return null;

    }
}
