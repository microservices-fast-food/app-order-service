package ai.ecma.apporderservice.controller;

import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.bonus.SaleBonusReqDto;
import ai.ecma.apporderservice.payload.bonus.SaleBonusResDto;
import ai.ecma.apporderservice.payload.bonus.SaleBonusViewDto;
import ai.ecma.apporderservice.utils.RestConstants;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RequestMapping(RestConstants.SALE_BONUS_CONTROLLER)
public interface SaleBonusController {

    @PostMapping
    ApiResult<SaleBonusResDto> add(@RequestBody SaleBonusReqDto saleBonusReqDto);

    @GetMapping("/{id}")
    ApiResult<SaleBonusViewDto> getOne(@PathVariable UUID id);

    @DeleteMapping("/{id}")
    ApiResult<?> delete(@PathVariable UUID id);

    /**
     *
     * @param id saleBonusning idsi
     * @param saleBonusReqDto
     * @return
     * maqsad:saleBonusni o'zgartirish
     * jarayonlar:
     *1.1. Berilgan startDate(bonusning boshlanish sanasi) fieldi endDate(bonusning tugashi sanasi)field dan katta  bo'lsa throw;
     *1.2. startDate fieldi endDate field ga tengligi tekshiriladi:
     *    1.2.1. startDate fieldi endDate field ga teng bo'lganda:
     *          1.2.1.1 berilgan startTIme field endTime field dan katta yoki teng  bo'lsa throw;
     *          1.2.1.2 berilgan startTIme field endTime field dan kichik  bo'lsa davom etamiz;
     *                  1.2.1.2.1 startTime(bonusning boshlanish soati) fieldi endTime(bonusning tugashi soati)field dan tekshiriladi:
     *                            1.2.1.2.1.1. startTime(bonusning boshlanish soati) fieldi endTime(bonusning tugashi soati)field ga teng bo'lsa yoki katta throw;
     *                            1.2.1.2.1.2. startTime(bonusning boshlanish soati) fieldi endTime(bonusning tugashi soati)field ga kichik bo'lsa:
     *                                          1.2.1.2.1.2.1.
     *
     */
    @PutMapping("/{id}")
    ApiResult<SaleBonusResDto> edit(@PathVariable UUID id,
                                    @RequestBody SaleBonusReqDto saleBonusReqDto);
}
