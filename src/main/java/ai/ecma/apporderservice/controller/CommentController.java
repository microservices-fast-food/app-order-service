package ai.ecma.apporderservice.controller;

import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.CommentReqDto;
import ai.ecma.apporderservice.payload.CommentResDto;
import ai.ecma.apporderservice.payload.CustomPage;
import ai.ecma.apporderservice.utils.AppConstant;
import ai.ecma.apporderservice.utils.RestConstants;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstants.COMMENT_CONTROLLER)
public interface CommentController {

    @GetMapping("/getAllByOrderId")
    ApiResult<List<CommentResDto>> getAllByOrderId(@PathVariable UUID id);

    @GetMapping
    ApiResult<CustomPage<CommentResDto>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                                @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);
    @GetMapping("/read")
    ApiResult<CustomPage<CommentResDto>> getAllRead(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                                    @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);
    @GetMapping("/unread")
    ApiResult<CustomPage<CommentResDto>> getAllUnread(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                                      @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @GetMapping("/{id}")
    ApiResult<CommentResDto> getComment(@PathVariable UUID id);

    @PostMapping()
    ApiResult<CommentResDto> add(@RequestBody @Valid CommentReqDto commentReqDto);

    @PutMapping("/{id}")
    ApiResult<CommentResDto> readComment(@PathVariable UUID id);

//    @DeleteMapping("/{id}")
//    ApiResult<?> delete(@PathVariable UUID id);
}
