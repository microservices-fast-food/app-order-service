package ai.ecma.apporderservice.service;



import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.CommentReqDto;
import ai.ecma.apporderservice.payload.CommentResDto;
import ai.ecma.apporderservice.payload.CustomPage;

import java.util.List;
import java.util.UUID;

public interface CommentService {

    ApiResult<List<CommentResDto>> getAllByOrderId(UUID id);

    ApiResult<CustomPage<CommentResDto>> getAll(int page, int size);

    ApiResult<CustomPage<CommentResDto>> getAllRead(int page, int size);

    ApiResult<CustomPage<CommentResDto>> getAllUnread(int page, int size);

    ApiResult<CommentResDto> getComment(UUID id);

    ApiResult<CommentResDto> addComment(CommentReqDto commentReqDto);

    ApiResult<CommentResDto> readComment(UUID id);

//    ApiResult<?> deleteComment(UUID id);


}
