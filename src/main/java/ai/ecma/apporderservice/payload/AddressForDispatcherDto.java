package ai.ecma.apporderservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressForDispatcherDto {
    private UUID id;

    private Double lat;

    private Double lon;

    private String fullAddress;
}
