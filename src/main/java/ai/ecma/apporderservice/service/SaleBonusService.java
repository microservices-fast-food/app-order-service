package ai.ecma.apporderservice.service;

import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.bonus.SaleBonusReqDto;
import ai.ecma.apporderservice.payload.bonus.SaleBonusResDto;
import ai.ecma.apporderservice.payload.bonus.SaleBonusViewDto;

import java.util.UUID;

public interface SaleBonusService {
    ApiResult<SaleBonusResDto> add(SaleBonusReqDto saleBonusReqDto);

    ApiResult<SaleBonusViewDto> getOne(UUID id);

    ApiResult<?> delete(UUID id);

    ApiResult<SaleBonusResDto> edit(UUID id, SaleBonusReqDto saleBonusReqDto);

}
