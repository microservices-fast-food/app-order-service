package ai.ecma.apporderservice.mapper;

import ai.ecma.appdblib.entity.order.bonus.DeliveryFreeBonus;
import ai.ecma.appdblib.entity.order.bonus.OrderBonus;
import ai.ecma.appdblib.entity.order.bonus.ProductPriceBonus;
import ai.ecma.appdblib.entity.order.bonus.SaleBonus;
import ai.ecma.apporderservice.feign.ProductFeign;
import ai.ecma.apporderservice.payload.OrderBonusBasketDto;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusBasketDto;
import ai.ecma.apporderservice.payload.bonus.ProductPriceBonusBasketDto;
import ai.ecma.apporderservice.payload.bonus.SaleBonusBasketDto;
import ai.ecma.apporderservice.service.ProductCountBonusService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class OrderBonusMapper {
    @Autowired
    ProductCountBonusService productCountBonusService;

    @Autowired
    ProductFeign productFeign;

    //bu metod productPriceBonus dan  ProductPriceBonusBasketDto ga o'giradi
    public abstract ProductPriceBonusBasketDto productPriceBonusToBasketDto(ProductPriceBonus productPriceBonus);

    //bu metod deliveryFreeBonus dan  DeliveryFreeBonusBasketDto ga o'giradi
    public abstract DeliveryFreeBonusBasketDto deliveryFreeBonusToBasketDto(DeliveryFreeBonus deliveryFreeBonus);

    @Mapping(target = "sale", expression = "java(productFeign.getOneSaleBasketDto(saleBonus.getSaleId()).getData())")
    public abstract SaleBonusBasketDto saleBonusToBasketDto(SaleBonus saleBonus);


    //
    @Mappings({
            @Mapping(target = "productPriceBonus", expression = "java(productPriceBonusToBasketDto(orderBonus.getProductPriceBonus()))"),
            @Mapping(target = "productCountBonus", expression = "java(productCountBonusService.productCountBonusToBasketDto(orderBonus.getProductCountBonus()))"),
            @Mapping(target = "deliveryFreeBonus", expression = "java(deliveryFreeBonusToBasketDto(orderBonus.getDeliveryFreeBonus()))"),
            @Mapping(target = "saleBonus", expression = "java(saleBonusToBasketDto(orderBonus.getSaleBonus()))")

    })
    public abstract OrderBonusBasketDto orderBonusToBasketDto(OrderBonus orderBonus);
}
