package ai.ecma.apporderservice.mapper;

import ai.ecma.appdblib.entity.order.bonus.ProductCountBonus;
import ai.ecma.apporderservice.payload.bonus.ProductCountBonusAddDto;
import ai.ecma.apporderservice.payload.bonus.ProductCountBonusDisplayDto;
import ai.ecma.apporderservice.payload.bonus.ProductCountBonusInfoDto;
import ai.ecma.apporderservice.utils.CommonUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring", imports = CommonUtils.class)
public interface ProductCountBonusMapper {

    ProductCountBonus productCountBonusAddDtoToProductCountBonus(
            ProductCountBonusAddDto productCountBonusAddDto
    );

    ProductCountBonusDisplayDto productCountBonusToProductCountBonusDisplayDto(
            ProductCountBonus productCountBonus
    );

    @Mapping(target = "productName",expression = "java(getProductById(productId, productCountBonusList).getName())")
    @Mapping(target = "giftProductName",expression = "java(getProductById(giftProductId, productCountBonusList).getName())")
    @Mapping(target = "photoUrl",expression = "java(getProductById(productId, productCountBonusList).getPhotoUrl())")
    List<ProductCountBonusInfoDto> productCountBonusToInfoDtoList(
            List<ProductCountBonus> productCountBonusList);

    void updateProductCountBonus(
            ProductCountBonusAddDto productCountBonusAddDto,
            @MappingTarget ProductCountBonus productCountBonus);

}
