package ai.ecma.apporderservice.payload.bonus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductPriceBonusBasketDto {
    private UUID id;
    private String name;
}
