package ai.ecma.apporderservice.controller;

import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.CustomPage;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusAddDto;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusBasketDto;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusGetForAdmin;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeEditDto;
import ai.ecma.apporderservice.service.DeliveryFreeBonusService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class DeliveryFreeBonusControllerImpl implements DeliveryFreeBonusController{
    private final DeliveryFreeBonusService deliveryFreeBonusService;

    @Override
    public ApiResult<CustomPage<DeliveryFreeBonusBasketDto>> getAll(int page, int size) {
        return deliveryFreeBonusService.getAll(page, size);
    }

    @Override
    public ApiResult<CustomPage<DeliveryFreeBonusGetForAdmin>> getForAdmin(int page, int size) {
        return deliveryFreeBonusService.getForAdmin(page, size);
    }

    @Override
    public ApiResult<DeliveryFreeBonusBasketDto> getOne(UUID id) {
        return deliveryFreeBonusService.getOne(id);
    }

    @Override
    public ApiResult<DeliveryFreeBonusBasketDto> add(DeliveryFreeBonusAddDto deliveryFreeBonusAddDto) {
        return deliveryFreeBonusService.add(deliveryFreeBonusAddDto);
    }

    @Override
    public ApiResult<DeliveryFreeBonusBasketDto> edit(DeliveryFreeEditDto deliveryFreeEditDto, UUID id) {
        return deliveryFreeBonusService.edit(deliveryFreeEditDto, id);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        return deliveryFreeBonusService.delete(id);
    }
}
