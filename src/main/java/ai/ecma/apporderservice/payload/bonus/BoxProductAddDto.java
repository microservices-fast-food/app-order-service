package ai.ecma.apporderservice.payload.bonus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BoxProductAddDto {
    @NotNull(message = "Mahsulot bo'sh bo'lmasligi kerak")
    private UUID productId;
    @NotBlank(message = "Box nomi bo'sh bo'lmasligi kerak")
    private String boxName;
}
