package ai.ecma.apporderservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderForOperatorDto {

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private Double price;
    private Double deliveryPrice;
    private Long code;
}
