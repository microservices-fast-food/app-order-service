package ai.ecma.apporderservice.payload.bonus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.sql.Date;
import java.sql.Time;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaleBonusReqDto {

    @NotBlank(message = "Name bo'sh bo'lmasligi kerak")
    private String name;

    @NotNull(message = "Start time bo'sh bo'lmasligi kerak")
    private Time startTime;

    @NotNull(message = "Start time bo'sh bo'lmasligi kerak")
    private Time endTime;

    //
    @FutureOrPresent(message = "Boshlanish sanasi hozirgi yoki kelasi sana bo'lshi kerak")
    @NotNull(message = "Start time bo'sh bo'lmasligi kerak")
    private Date startDate;

    @Future(message = "Tugash sanasi hozirgi sanadan keyin bo'lishi kerak")
    @NotNull(message = "Start time bo'sh bo'lmasligi kerak")
    private Date endDate;

    @Positive(message = "Min price 0 dan yuqori bo'lishi kerak")
    private Double minPrice;

    @NotNull(message = "Sale bo'sh bo'lmasligi kerak")
    private UUID saleId;

}
