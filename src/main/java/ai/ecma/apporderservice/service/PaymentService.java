package ai.ecma.apporderservice.service;

import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.PayDto;

public interface PaymentService {
    ApiResult<?> paymentProcess(PayDto payDto);
}
