package ai.ecma.apporderservice.feign;

import ai.ecma.apporderservice.config.FeignConfig;
import ai.ecma.apporderservice.payload.AddressCourierDto;
import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.utils.RestConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.UUID;

@FeignClient(name = RestConstants.AUTH_SERVICE, configuration = FeignConfig.class)
public interface AddressFeign {

    /**
     * Bu yo'lga addresslarning IdListini bersa bizga addressResDto List qaytaradi.
     * @param addressIds @RequestBody
     * @return ApiResult<List<AddressDto>>
     */
    @PostMapping("/get-addresses-by-ids")
    ApiResult<List<AddressCourierDto>> addressesByIds(@RequestBody List<UUID> addressIds);

    @GetMapping("/{id}")
    ApiResult<AddressCourierDto> getOneAddress(@PathVariable(name = "id") UUID id);
}
