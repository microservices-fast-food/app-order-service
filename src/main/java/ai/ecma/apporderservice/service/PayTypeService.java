package ai.ecma.apporderservice.service;

import ai.ecma.appdblib.entity.enums.PayTypeEnum;
import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.PayTypeResDto;

import java.util.List;
import java.util.UUID;

public interface PayTypeService {
    ApiResult<List<PayTypeResDto>> getAll();

    ApiResult<List<PayTypeResDto>> getFilter(PayTypeEnum type);

    ApiResult<PayTypeResDto> getOne(UUID id);
}
