package ai.ecma.apporderservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddOrRemoveProductDto {
    @NotNull(message = "add bo'sh bo'lmasligi kerak")
    private Boolean add;

    @NotNull(message = "update bo'sh bo'lmasligi kerak")
    private Boolean update;

    @NotNull(message = "productId bo'sh bo'lmasligi kerak")
    private UUID productId;
}
