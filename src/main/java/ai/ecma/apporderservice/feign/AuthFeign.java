package ai.ecma.apporderservice.feign;

import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.user.User;
import ai.ecma.apporderservice.config.FeignConfig;
import ai.ecma.apporderservice.payload.AddressForDispatcherDto;
import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.UserDto;
import ai.ecma.apporderservice.payload.UserResDto;
import ai.ecma.apporderservice.utils.RestConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@FeignClient(name = RestConstants.AUTH_SERVICE, configuration = FeignConfig.class)
public interface AuthFeign {

    @GetMapping("/auth/check-user")
    ApiResult<User> checkUser(@RequestHeader("Authorization") String token);

    @PostMapping("/auth/check-permission")
    ApiResult<User> checkPermission(@RequestHeader("Authorization") String token,
                                    @RequestBody PermissionEnum permissionEnum);

    @GetMapping("/{id}")
    ApiResult<UserResDto> getOne(@PathVariable(name = "id") UUID id);

    @GetMapping("/{id}")
    ApiResult<AddressForDispatcherDto> getOneAddress(@PathVariable(name = "id") UUID id);

    /**
     * Bu yo'l hali yozilmagan.
     * Ushbu yo'lga UserIdslarni berilsa bizga Userlarni listini qaytaradi
     * @param userIdList @RequestBody
     * @return ApiResult<List<UserResDto>>
     */
   @PostMapping("/users-by-ids")
    ApiResult<List<UserResDto>> usersByIds(@RequestBody List<UUID> userIdList);

    @PostMapping("/get-addresses-by-ids")
    ApiResult<List<AddressForDispatcherDto>> getAddressesByIds(@RequestBody Set<UUID> ids);

    @GetMapping("/get-users-by-ids")
    ApiResult<List<UserDto>>  getUsersByIds(@RequestBody Set<UUID> ids);

}
