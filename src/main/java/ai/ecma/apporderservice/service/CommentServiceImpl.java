package ai.ecma.apporderservice.service;

import ai.ecma.appdblib.entity.enums.OrderStatusEnum;
import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.order.Comment;
import ai.ecma.appdblib.entity.order.Order;
import ai.ecma.appdblib.entity.user.User;
import ai.ecma.appdblib.repository.order.CommentRepository;
import ai.ecma.appdblib.repository.order.OrderRepository;
import ai.ecma.apporderservice.aop.annotation.CheckAuth;
import ai.ecma.apporderservice.component.MessageByLang;
import ai.ecma.apporderservice.exception.RestException;
import ai.ecma.apporderservice.mapper.CommentMapper;
import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.CommentReqDto;
import ai.ecma.apporderservice.payload.CommentResDto;
import ai.ecma.apporderservice.payload.CustomPage;
import ai.ecma.apporderservice.utils.CommonUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {
    private OrderRepository orderRepository;
    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;
    private final MessageByLang messageByLang;

    @CheckAuth(permission = PermissionEnum.VIEW_COMMENT )
    @Override
    public ApiResult<List<CommentResDto>> getAllByOrderId(UUID orderId) {
        List<Comment> commentsByOrderId = commentRepository.getAllByOrderId(orderId);
        List<CommentResDto> commentResDtos = commentMapper.commentListToResDtoList(commentsByOrderId);
        return ApiResult.successResponse(commentResDtos);
    }

    @CheckAuth(permission = PermissionEnum.VIEW_COMMENT )
    @Override
    public ApiResult<CustomPage<CommentResDto>> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Comment> commentPage = commentRepository.findAll(pageable);
        CustomPage<CommentResDto> customResPage = commentResDtoCustomPage(commentPage);
        return ApiResult.successResponse(customResPage);
    }

    @CheckAuth(permission = PermissionEnum.VIEW_COMMENT )
    @Override
    public ApiResult<CustomPage<CommentResDto>> getAllRead(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Comment> commentPage = commentRepository.getAllByUnreadIsFalse(pageable);
        CustomPage<CommentResDto> customResPage = commentResDtoCustomPage(commentPage);
        return ApiResult.successResponse(customResPage);
    }

    @CheckAuth(permission = PermissionEnum.VIEW_COMMENT )
    @Override
    public ApiResult<CustomPage<CommentResDto>> getAllUnread(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Comment> commentPage = commentRepository.getAllByUnreadIsTrue(pageable);
        CustomPage<CommentResDto> customResPage = commentResDtoCustomPage(commentPage);
        return ApiResult.successResponse(customResPage);
    }

    @Override
    public ApiResult<CommentResDto> getComment(UUID id) {
        Comment comment = commentRepository.findById(id)
                .orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("COMMENT_NOT_FOUND")));
        return ApiResult.successResponse(commentMapper.commentToResDto(comment));
    }

    @CheckAuth()
    @Override
    public ApiResult<CommentResDto> addComment(CommentReqDto commentReqDto) {
        User user = CommonUtils.getUserFromRequest();
        if (commentReqDto.getOrderId() != null) {
            Order order = orderRepository.findById(commentReqDto.getOrderId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Order topilmadi"));
            if (!order.getCustomerId().equals(user.getId()))
                throw new RestException(HttpStatus.BAD_REQUEST,messageByLang.getMessageByKey("ORDER_NOT_YOURS"));
            if (!order.getStatus().equals(OrderStatusEnum.CLOSED))
                throw new RestException(HttpStatus.BAD_REQUEST,messageByLang.getMessageByKey("ORDER_STATUS_NOT_CLOSED"));
        }

        Comment comment = commentMapper.commentReqDtoToComment(commentReqDto);
        commentRepository.save(comment);
        return ApiResult.successResponse(commentMapper.commentToResDto(comment));
    }

    @CheckAuth(permission = PermissionEnum.READ_COMMENT )
    @Override
    public ApiResult<CommentResDto> readComment(UUID id) {

        Comment comment = commentRepository.findById(id)
                .orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("COMMENT_NOT_FOUND")));
        comment.setUnread(false);
        commentRepository.save(comment);

        return ApiResult.successResponse(commentMapper.commentToResDto(comment));


    }



    public CustomPage<CommentResDto> commentResDtoCustomPage(Page<Comment> commentPage) {
        return new CustomPage<>(
                commentMapper.commentListToResDtoList(commentPage.getContent()),
                commentPage.getTotalPages(),
                commentPage.getNumber(),
                commentPage.getTotalElements(),
                commentPage.getSize(),
                commentPage.getNumberOfElements()
        );
    }
}
