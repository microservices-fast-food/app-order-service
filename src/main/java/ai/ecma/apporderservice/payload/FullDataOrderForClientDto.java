package ai.ecma.apporderservice.payload;

import ai.ecma.appdblib.entity.enums.OrderStatusEnum;
import ai.ecma.appdblib.entity.enums.OrderTypeEnum;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusBasketDto;
import ai.ecma.apporderservice.payload.bonus.SaleBasketDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FullDataOrderForClientDto {
    private UUID id;
    private Long code;
    private Timestamp createdAt;
    private String lastNameCustomer;
    private String lastNameCourier;
    private String fullAddress;
    private Double price;
    private Double deliveryPrice;
    private Double totalPrice;
    private List<ProductForClientViewDto> products;
    private OrderStatusEnum status;
    private OrderTypeEnum type;
    private SaleBasketDto saleBonus;
    private DeliveryFreeBonusBasketDto deliveryBonus;
}
