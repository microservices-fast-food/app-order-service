package ai.ecma.apporderservice.payload;

import ai.ecma.appdblib.entity.enums.PayTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PayTypeResDto {
    private UUID id;
    private String name;
    private PayTypeEnum payType;
}
