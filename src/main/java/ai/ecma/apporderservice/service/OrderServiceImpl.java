package ai.ecma.apporderservice.service;

import ai.ecma.appdblib.entity.enums.*;
import ai.ecma.appdblib.entity.order.*;
import ai.ecma.appdblib.entity.order.bonus.*;
import ai.ecma.appdblib.entity.user.User;
import ai.ecma.appdblib.repository.order.*;
import ai.ecma.apporderservice.aop.annotation.CheckAuth;
import ai.ecma.apporderservice.component.MessageByLang;
import ai.ecma.apporderservice.exception.RestException;
import ai.ecma.apporderservice.feign.AddressFeign;
import ai.ecma.apporderservice.feign.AuthFeign;
import ai.ecma.apporderservice.feign.ProductFeign;
import ai.ecma.apporderservice.mapper.DeliveryFreeBonusMapper;
import ai.ecma.apporderservice.mapper.OrderMapper;
import ai.ecma.apporderservice.mapper.ProductMapper;
import ai.ecma.apporderservice.payload.*;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusBasketDto;
import ai.ecma.apporderservice.payload.bonus.ProductResDto;
import ai.ecma.apporderservice.payload.bonus.SaleBasketDto;
import ai.ecma.apporderservice.payload.bonus.SaleResDto;
import ai.ecma.apporderservice.utils.CommonUtils;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final OrderProductRepository orderProductRepository;
    private final ProductPriceBonusRepository productPriceBonusRepository;
    private final OrderBonusRepository orderBonusRepository;
    private final BoxProductRepository boxProductRepository;
    private final DeliveryFreeBonusRepository deliveryFreeBonusRepository;
    private final ProductCountBonusRepository productCountBonusRepository;
    private final SaleBonusRepository saleBonusRepository;
    private final PayTypeRepository payTypeRepository;
    private final OrderHistoryRepository orderHistoryRepository;
    private final PropertyRepository propertyRepository;
    private final CourierOrderRepository courierOrderRepository;

    private final ProductFeign productFeign;
    private final AuthFeign authFeign;
    private final AddressFeign addressFeign;

    private final OrderMapper orderMapper;
    private final ProductMapper productMapper;
    private final DeliveryFreeBonusMapper deliveryFreeBonusMapper;

    private final MessageByLang messageByLang;

    @Value("${kelishilganSana}")
    private String kelishilganSana;

    @Value("${kelishilganSanaPlusKun}")
    private String kelishilganSanaPlusKun;

    //Savatchaga order qo'shish yoki o'chirish uchun
    @SneakyThrows
    @CheckAuth
    @Override
    public ApiResult<OrderBasketDto> addOrRemoveProduct(AddOrRemoveProductDto addOrRemoveProductDto) {
        User customer = CommonUtils.getUserFromRequest();
        ProductBasketDto productBasketDto = productFeign.getProductById(addOrRemoveProductDto.getProductId()).getData();
        Optional<Order> optionalOrder = orderRepository.findByCustomerIdAndStatus(customer.getId(), OrderStatusEnum.DRAFT);
        Order order;
        OrderProduct orderProduct;
        if (optionalOrder.isPresent()) {
            order = optionalOrder.get();
            addOrRemoveProduct(productBasketDto, order.getId(), addOrRemoveProductDto.getAdd());
        } else {
            order = new Order(
                    customer.getId(),
                    OrderStatusEnum.DRAFT,
                    productBasketDto.getPrice()
            );

            orderProduct = new OrderProduct(
                    order.getId(),
                    productBasketDto.getId(),
                    productBasketDto.getPrice(),
                    1
            );
            orderProductRepository.save(orderProduct);
        }


        //update tru bo'lsa savatchadagi mahsulotlar narxi va bonuslar qaytadan hisoblanadi
        if (addOrRemoveProductDto.getUpdate()) {

            //hozirdagi eng oxirgi orderni umumiy narxini topib olamiz
            Double price = orderProductRepository.sumByOrderId(order.getId());
            order.setPrice(price);

            OrderBonus orderBonus = orderBonusRepository.findByOrderId(order.getId()).orElse(new OrderBonus());

            //shu orderga hozirda productPriceBonus tushishi tekshiramiz va tushsa orderBonusga biriktiramiz
            checkProductPriceBonusAndUpdateOrderBonus(order.getPrice(), orderBonus);

            //Ushbu metod yetkazib berish narxini tekshiradi va bo'lsa orderBonusga qo'shadi
            checkDeliveryFreeBonusAndUpdateOrderBonus(order.getPrice(), orderBonus);

            // Bu metod buyurtmadagi productlarni sonini tekshiradi,
            // agar product soniga oid bonus mavjud bo'lsa, orderBonus ga
            // productCountBonus ni biriktiradi
            checkProductCountBonusAndUpdateOrderBonus(order.getId(), orderBonus);

            // Ushbu method berilgan orderga mos keladigan saleBonuslarni qidiradi va agar bulsa order ni price dan sale ning amount ni chegirib tashlab orderBonus ga ushbu saleBonusni set qiladi
            checkSaleBonusAndUpdateOrderBonus(order, orderBonus);


            orderBonus.setOrderId(order.getId());
            orderBonusRepository.save(orderBonus);

            //qaytaruvchi dtoni ushbu metod yordamizda yasaymiz
            OrderBasketDto orderBasketDto = createApiResponse(order);
            orderRepository.save(order);

            return ApiResult.successResponse(orderBasketDto);
        }
        orderRepository.save(order);
        return ApiResult.successResponse(" ");
    }

    //savatchani tozlash uchun
    @CheckAuth
    @Override
    public ApiResult<?> clearBasket() {
        User customer = CommonUtils.getUserFromRequest();
        Optional<Order> optionalOrder = orderRepository.findByCustomerIdAndStatus(customer.getId(), OrderStatusEnum.DRAFT);
        if (optionalOrder.isPresent()) {
            Order order = optionalOrder.get();
            orderProductRepository.deleteAllByOrderId(order.getId());
            return ApiResult.successResponse(messageByLang.getMessageByKey("BASKET_CLEAR"));
        }
        return ApiResult.successResponse("OK");
    }

    //agar qaysidur mahsulot narxini o'zgartirilsa ushbu metodga kelib barcha DRAFT orderlada narxni boshqattan hisoblaydi
    @Override
    public ApiResult<?> updateOrderProductPrice(OrderUpdatePriceDto orderUpdatePriceDto) {
        //mahsulotni narxi o'zgarsa ushbu metodga yangi narx sale bilan hisoblangan oxirgi narx va mahsulot id si keladi
        //biz shu mahsulot kimda savatchasida bo'lsa shulani order productiga yangi narxga o;zgartirib chiqamiz
        orderProductRepository.updatePriceOrderProduct(orderUpdatePriceDto.getFinalPrice(), OrderStatusEnum.DRAFT, orderUpdatePriceDto.getProductId());

        //orderproductda narxla yangilangandan keyin umumiy har bir savatchani narxi o'zgartiriladi
        orderRepository.updatePriceOrder(OrderStatusEnum.DRAFT);
        return ApiResult.successResponse("OK");
    }

    //Order ga box(ProductPriceBonus berilganda box tanlash imkoni beriladi) biriktirish yani client tanlagan box
    @CheckAuth
    @Override
    public ApiResult<?> addBoxNameToOrder(String boxName) {
        //Darft orderni current user orqali topamiz
        User customer = CommonUtils.getUserFromRequest();
        Order order = orderRepository.findByCustomerIdAndStatus(customer.getId(), OrderStatusEnum.DRAFT).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Savatcha topilmadi"));

        UUID productPriceBonusId = orderBonusRepository.getProductPriceBonusId(order.getId());

        boolean existsByBoxNameAndProductPriceBonusId = boxProductRepository.existsByBoxNameAndProductPriceBonusId(boxName, productPriceBonusId);
        if (!existsByBoxNameAndProductPriceBonusId)
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("BOX_NAME_NOT_FOUNT"));


        //boxName va order id ni orderBonus tablega yozadi
        orderBonusRepository.updateBoxName(boxName, order.getId());
        return ApiResult.successResponse("OK");
    }

    //order ni tasdiqlash client tanlab to;ldirgan ma'lumotlani tekshirib orderga set qilish
    @CheckAuth
    @Transactional
    @Override
    public ApiResult<?> orderConfirm(OrderConfirmDto orderConfirmDto) {
        User user = CommonUtils.getUserFromRequest();
        Order order = orderRepository.findById(orderConfirmDto.getId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ORDER_NOT_FOUND")));
        order.setAddressId(orderConfirmDto.getAddressId());
        order.setBranchId(orderConfirmDto.getBranchId());
        order.setPlanReceivedTime(orderConfirmDto.getPlanReceivedTime());
        order.setDistance(orderConfirmDto.getDistance());


        PayType payType = payTypeRepository.findById(orderConfirmDto.getPayTypeId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PAY_TYPE_NOT_FOUND")));

        if (payType.getPayType().equals(PayTypeEnum.CASH)) {

            Optional<Property> optionalProperty = propertyRepository.findByKey("reliabilityRate");
            if (optionalProperty.isPresent()) {
                Property property = optionalProperty.get();
                if (Integer.parseInt(property.getValue()) <= user.getReliabilityRate()) {
                    order.setStatus(OrderStatusEnum.ACCEPTED);
                } else
                    order.setStatus(OrderStatusEnum.PENDING);
            } else
                order.setStatus(OrderStatusEnum.PENDING);


            OrderHistory orderHistory = new OrderHistory(
                    order.getId(),
                    user.getId(),
                    order.getStatus()
            );

            orderHistoryRepository.save(orderHistory);
        }
        order.setPayTypeId(orderConfirmDto.getPayTypeId());
        Long code = generateOrderCode();
        order.setCode(code);
        orderRepository.save(order);
        return ApiResult.successResponse("OK");
    }

    //order ga branch tanlash uchun branch lani eng yaqinlarini tanlsh uchun chiqarib berish
    @CheckAuth
    @Override
    public ApiResult<List<BranchInfoForClientDto>> orderSelectBranch(FindNearestBranchDto findNearestBranchDto) {
        User user = CommonUtils.getUserFromRequest();
        Order order = orderRepository.findByCustomerIdAndStatus(user.getId(), OrderStatusEnum.DRAFT).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ORDER_NOT_FOUND")));

        List<BranchInfoForClientDto> result = new ArrayList<>();

        if (findNearestBranchDto.getType().equals(OrderTypeEnum.TAKE_AWAY)) {
            order.setDeliveryPrice(0d);
            order.setType(OrderTypeEnum.TAKE_AWAY);

            List<BranchInfoForClientDto> branches = productFeign.findNearestBranchs(findNearestBranchDto).getData();
            result.addAll(branches);
        } else {
            OrderBonus orderBonus = orderBonusRepository.findByOrderId(order.getId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ORDER_BONUS_NOT_FOUND")));
            BranchInfoForClientDto branch = productFeign.findNearestBranch(findNearestBranchDto).getData();

            order.setDistance(branch.getDistance());
            order.setBranchId(branch.getId());

            if (orderBonus.getDeliveryFreeBonusId() != null)
                order.setDeliveryPrice(0d);
            else
                order.setDeliveryPrice(CommonUtils.priceRoundDigit(branch.getDeliveryPrice()));

            order.setType(OrderTypeEnum.DELIVERY);

            result.add(branch);
        }
        orderRepository.save(order);

        return ApiResult.successResponse(result);
    }

    @CheckAuth(permission = PermissionEnum.VIEW_ORDER)
    //branch bo'yicha courier biriktirilmagan orderlani qayataradi
    @Override
    public ApiResult<List<OrderForDispatcherDto>> getOrdersByBranchId(UUID branchId) {
        List<Order> orders = orderRepository.findAllByCourierIdNullAndBranchIdAndStatusAndType(branchId, OrderStatusEnum.KITCHEN, OrderTypeEnum.DELIVERY);
        if (orders.size() == 0)
            return ApiResult.successResponse(new ArrayList<>());

        Set<UUID> customerIds = new HashSet<>();
        Set<UUID> addressIds = new HashSet<>();
        for (Order order : orders) {
            customerIds.add(order.getCustomerId());
            addressIds.add(order.getAddressId());
        }

        List<AddressForDispatcherDto> addresses = authFeign.getAddressesByIds(addressIds).getData();
        if (addresses.size() != addressIds.size())
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ADDRESS_NOT_FOUND"));

        List<UserDto> customers = authFeign.getUsersByIds(customerIds).getData();
        if (customers.size() != customerIds.size())
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("CUSTOMER_NOT_FOUND"));

        HashMap<UUID, AddressForDispatcherDto> hashMapAddressIdAndAddress = createHashMapAddressIdAndAddress(addresses);
        HashMap<UUID, UserDto> hashMapCustomerIdAndCustomer = createHashMapCustomerIdAndCustomer(customers);
        List<OrderForDispatcherDto> orderForDispatcherDtos = orderMapper.orderToDispatcherDtoList(orders, hashMapCustomerIdAndCustomer, hashMapAddressIdAndAddress);

        return ApiResult.successResponse(orderForDispatcherDtos);
    }

    @CheckAuth(permission = PermissionEnum.ATTACH_COURIER_ORDER)
    //orderga courier ni biriktirish
    @Override
    public ApiResult<String> addCourierToOrder(AddCourierToOrderDto addCourierToOrderDto) {
        Order order = orderRepository.findById(addCourierToOrderDto.getOrderId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ORDER_NOT_FOUND")));
        CourierOrder courierOrder = courierOrderRepository.findByCloseFalseAndCourierId(addCourierToOrderDto.getCourierId()).orElse(new CourierOrder());

        courierOrder.setCourierId(addCourierToOrderDto.getCourierId());
        String[] orders = courierOrder.getOrders();

        String[] newOrders = new String[orders.length + 1];
        if (orders.length != 0) {

            for (int i = 0; i < newOrders.length; i++) {
                if (orders[i].equals(addCourierToOrderDto.getOrderId().toString()))
                    throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("CONFLICT_ADD_COURIER_TO_ORDER"));
                newOrders[i] = orders[i];
            }
            newOrders[newOrders.length - 1] = order.getId().toString();
        } else
            newOrders[0] = order.getId().toString();

        courierOrder.setOrders(newOrders);
        courierOrder.setClose(addCourierToOrderDto.isClose());
        courierOrderRepository.save(courierOrder);

        order.setCourierId(addCourierToOrderDto.getCourierId());
        orderRepository.save(order);

        return ApiResult.successResponse("");
    }

    //Courier uchun order lani olish
    @CheckAuth
    @Override
    public ApiResult<CourierOrderBoxDto> getOrderForCourier() {
        User user = CommonUtils.getUserFromRequest();
        Optional<CourierOrder> allByCloseAndCreatedAtDesc = courierOrderRepository.findAllByCloseAndCreatedAtDesc(user.getId());

        //Agar courierOrder bo'sh bo'lsa, Bo'sh narsa qaytaramiz
        if (allByCloseAndCreatedAtDesc.isEmpty())
            return ApiResult.successResponse(new CourierOrderBoxDto());
        CourierOrder courierOrder = allByCloseAndCreatedAtDesc.get();
        CourierOrderBoxDto courierOrderBoxDto = courierOrderToCourierOrderBoxDto(courierOrder, user);
        return ApiResult.successResponse(courierOrderBoxDto);
    }

    @CheckAuth
    @Override
    public ApiResult<List<CourierOrderBoxDto>> getHistoryOrderListForCourier() {
        User user = CommonUtils.getUserFromRequest();
        List<CourierOrder> courierOrderList = courierOrderRepository.findAllByCloseOrderByCreatedAt(user.getId());
        //Agar courierOrder bo'sh bo'lsa, Bo'sh narsa qaytaramiz
        if (courierOrderList.isEmpty())
            return ApiResult.successResponse(new ArrayList<>());
        List<CourierOrderBoxDto> boxDtos = new ArrayList<>();
        for (CourierOrder courierOrder : courierOrderList) {
            CourierOrderBoxDto courierOrderBoxDto = courierOrderToCourierOrderBoxDto(courierOrder, user);
            boxDtos.add(courierOrderBoxDto);
        }
        return ApiResult.successResponse(boxDtos);
    }


    @CheckAuth
    @Override
    public ApiResult<String> editOrderStatus(UUID courierOrderId) {
        User user = CommonUtils.getUserFromRequest();

        //Id va courierId orqali CourierOrderni olish
        CourierOrder courierOrder = courierOrderRepository.findAllByIdAndCourierId(courierOrderId, user.getId())
                .orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("COURIER_NO_ORDER")));
        String[] orders = courierOrder.getOrders();
        Set<UUID> orderIds = Arrays.stream(orders).map(UUID::fromString).collect(Collectors.toSet());

        //OrderStatusni boshqa statusga o'zgartirish
        orderRepository.updateOrderStatusByIds(OrderStatusEnum.ON_THE_WAY, orderIds);

        List<OrderHistory> orderHistories = new ArrayList<>();
        for (UUID orderId : orderIds) {
            orderHistories.add(new OrderHistory(
                    orderId,
                    user.getId(),
                    OrderStatusEnum.ON_THE_WAY
            ));
        }
        orderHistoryRepository.saveAll(orderHistories);
        return ApiResult.successResponse("OK");
    }


    //mijozni o'zi uchun order tanlaganda batafsil order ma'lumotlarini chiqarib berish
    @CheckAuth
    @Override
    public ApiResult<FullDataOrderForClientDto> getFullDataOrderForClient(UUID orderId) {
        Order order = orderRepository.findById(orderId).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "order topilmadi"));
        User user = CommonUtils.getUserFromRequest();
        if (!order.getCustomerId().equals(user.getId()))
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("ORDER_NOT_YOURS"));

        HashMap<String, Integer> productIdAndCount = orderProductRepository.getProductIdAndCount(orderId);
        HashMap<UUID, Integer> hashMapProductCount = stringToUuidHashMap(productIdAndCount);

        Set<String> productIds = productIdAndCount.keySet();

        Optional<OrderBonus> optionalOrderBonus = orderBonusRepository.findByOrderId(orderId);
        SaleBasketDto saleBasketDto = new SaleBasketDto();
        DeliveryFreeBonusBasketDto deliveryFreeBonusBasketDto = new DeliveryFreeBonusBasketDto();
        Set<String> bonusProductIds = new HashSet<>();
        if (optionalOrderBonus.isPresent()) {
            OrderBonus orderBonus = optionalOrderBonus.get();



            if (orderBonus.getProductPriceBonusId() != null) {
                Set<String> productIdsByBoxName = boxProductRepository.getProductIdsByBoxNameAndProductPriceBonusId(orderBonus.getProductPriceBonusId(), orderBonus.getBoxName());

                HashMap<UUID, Integer> boxBonusHashMap = new HashMap<>();
                for (String s : productIdsByBoxName) {
                    boxBonusHashMap.put(UUID.fromString(s), 1);
                }
                bonusProductIds.addAll(productIdsByBoxName);

                hashMapProductCount.putAll(boxBonusHashMap);
            }
            if (orderBonus.getProductCountBonusId() != null) {
                HashMap<String, Integer> giftProductIdsAndCount = productCountBonusRepository.getGiftProductIdAndCount(orderBonus.getProductCountBonusId());
                HashMap<UUID, Integer> hashMapCountBonus = stringToUuidHashMap(giftProductIdsAndCount);
                bonusProductIds.addAll(giftProductIdsAndCount.keySet());
                hashMapProductCount.putAll(hashMapCountBonus);
            }

            productIds.addAll(bonusProductIds);
          if (orderBonus.getSaleBonusId()!=null){
              Optional<SaleBonus> optionalSaleBonus = saleBonusRepository.findById(orderBonus.getSaleBonusId());
              if (optionalSaleBonus.isPresent()) {
                  SaleBonus saleBonus = optionalSaleBonus.get();
                  saleBasketDto = productFeign.getOneSaleBasketDto(saleBonus.getSaleId()).getData();
              }
          }


           if (orderBonus.getDeliveryFreeBonusId()!=null){
               Optional<DeliveryFreeBonus> optionalDeliveryFreeBonus = deliveryFreeBonusRepository.findById(orderBonus.getDeliveryFreeBonusId());

               if (optionalDeliveryFreeBonus.isPresent()){
                   DeliveryFreeBonus deliveryFreeBonus = optionalDeliveryFreeBonus.get();
                   deliveryFreeBonusBasketDto = deliveryFreeBonusMapper.deliveryFreeBonusToResDto(deliveryFreeBonus);
               }
           }
        }

        Set<UUID> productIdsUuid = productIds.stream().map(UUID::fromString).collect(Collectors.toSet());
        List<ProductResDto> productResDtos = productFeign.getProductsByIds(productIdsUuid).getData();

        UserResDto courier = authFeign.getOne(order.getCourierId()).getData();
        AddressForDispatcherDto address = authFeign.getOneAddress(order.getAddressId()).getData();

        List<ProductForClientViewDto> productForClientViewDtos = productMapper.productResDtosToProductForClientViewDtoList(productResDtos, hashMapProductCount, bonusProductIds);

        FullDataOrderForClientDto fullDataOrderForClientDto = new FullDataOrderForClientDto(
                orderId,
                order.getCode(),
                order.getCreatedAt(),
                user.getLastName(),
                courier.getLastName(),
                address.getFullAddress(),
                order.getPrice(),
                order.getDeliveryPrice(),
                order.getPrice() + order.getDeliveryPrice(),
                productForClientViewDtos,
                order.getStatus(),
                order.getType(),
                saleBasketDto,
                deliveryFreeBonusBasketDto

        );


        return ApiResult.successResponse(fullDataOrderForClientDto);
    }

    @CheckAuth
    @Override
    public ApiResult<String> addOrderAndDeliveryRate(AddOrderDeliveryRate addOrderDeliveryRate) {
        Order order = orderRepository.findById(addOrderDeliveryRate.getOrderId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "order topilmadi"));
        User user = CommonUtils.getUserFromRequest();
        if (!order.getCustomerId().equals(user.getId()))
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("ORDER_NOT_YOURS"));
        if (!order.getStatus().equals(OrderStatusEnum.CLOSED))
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("ORDER_STATUS_NOT_CLOSED"));

        if (order.getType().equals(OrderTypeEnum.DELIVERY))
            order.setDeliveryRate(addOrderDeliveryRate.getDeliveryRate());


        order.setOrderRate(addOrderDeliveryRate.getOrderRate());
        orderRepository.save(order);
        return ApiResult.successResponse("OK");
    }

    //Client lar uchun order lani spiskasini olish
    @CheckAuth
    @Override
    public ApiResult<CustomPage<ViewOrdersForClientDto>> viewOrdersForClient(int page, int size) {
        User user = CommonUtils.getUserFromRequest();
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "createdAt"));
        Page<Order> pageOrders = orderRepository.findAllByCustomerIdAndStatusNot(user.getId(), OrderStatusEnum.DRAFT, pageable);
        CustomPage<ViewOrdersForClientDto> customPageOrders = orderForClientDtoCustomPage(pageOrders);
        return ApiResult.successResponse(customPageOrders);
    }

    //courier yetkazib bergandan keyin order statusini CLOSED ga o'tkazish
    @CheckAuth
    @Override
    public ApiResult<?> courierChangeOrderStatusToClosed(UUID courierOrderId) {
        User user = CommonUtils.getUserFromRequest();
        CourierOrder courierOrder = courierOrderRepository.findByCourierIdAndId(user.getId(), courierOrderId).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("COURIER_NO_ORDER")));

        String[] orders = courierOrder.getOrders();
        Set<UUID> orderIds = Arrays.stream(orders).map(UUID::fromString).collect(Collectors.toSet());
        orderRepository.updateOrderStatusByIds(OrderStatusEnum.CLOSED, orderIds);

        List<OrderHistory> orderHistories = new ArrayList<>();
        for (UUID orderId : orderIds) {
            orderHistories.add(new OrderHistory(
                    orderId,
                    user.getId(),
                    OrderStatusEnum.CLOSED
            ));
        }

        orderHistoryRepository.saveAll(orderHistories);

        return ApiResult.successResponse("OK");
    }

    //order statusini operator  ACCEPTED yoki REJECT ga o'zgartiradi
    @CheckAuth(permission = PermissionEnum.ACCEPT_OR_REJECT_ORDER)
    @Override
    public ApiResult<?> orderEditByOperator(OrderStatusEnum status, UUID orderId) {
        User user = CommonUtils.getUserFromRequest();

        //Status ACCEPTED yoki REJECT emasligini tekshiramiz
        if (!(status.equals(OrderStatusEnum.ACCEPTED) || status.equals(OrderStatusEnum.REJECT)))
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("BAD_REQUEST_ORDER_STATUS"));
        Order order = orderRepository.findById(orderId).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ORDER_NOT_FOUND")));

        //Status PENDING ligini tekshiramiz
        if (order.getStatus().equals(OrderStatusEnum.PENDING))
            order.setStatus(status);
        order.setCourierId(user.getId());
        orderRepository.save(order);
        OrderHistory orderHistory = new OrderHistory(
                order.getId(),
                user.getId(),
                status
        );
        orderHistoryRepository.save(orderHistory);
        UserResDto customer = authFeign.getOne(order.getCustomerId()).getData();
//        OrderForOperatorDto orderForOperatorDto=new OrderForOperatorDto(
//                customer.getFirstName(),
//                customer.getLastName(),
//                customer.getPhoneNumber(),
//                order.getPrice(),
//                order.getDeliveryPrice(),
//                order.getCode()
//        );
        return ApiResult.successResponse(orderMapper.orderForOperatorToDto(order, customer));
    }


    @CheckAuth(permission = PermissionEnum.VIEW_ORDER)
    //Hamma PENDING statusli order lani operator uchun page qilib order lani qaytarish
    @Override
    public ApiResult<CustomPage<OrderForOperatorDto>> getAllForOperator(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Order> orderPage = orderRepository.findAllByStatus(OrderStatusEnum.PENDING, pageable);
        CustomPage<OrderForOperatorDto> operatorDtoCustomPage = orderForOperatorDtoCustomPage(orderPage);
        return ApiResult.successResponse(operatorDtoCustomPage);
    }

    //savatchaga mahsulot qo'shish yoki o'chirish ni hisoblash
    private void addOrRemoveProduct(ProductBasketDto productBasketDto, UUID orderId, boolean add) {

        //agar mahsulot qo'shmoqchi bo'sa shu metodga kiradi
        if (add) {
            Optional<OrderProduct> optionalOrderProduct = orderProductRepository.findByOrderIdAndProductId(orderId, productBasketDto.getId());
            //huddi shu orderli id lik va product idlik orderProduct bo'lsa sonini 1 taga oshiramiz
            if (optionalOrderProduct.isPresent()) {
                OrderProduct orderProduct = optionalOrderProduct.get();
                orderProduct.setCount(orderProduct.getCount() + 1);
                Integer count = orderProduct.getCount();
                Double price = count * productBasketDto.getPrice();
                orderProduct.setPrice(CommonUtils.priceRoundDigit(price));
                orderProductRepository.save(orderProduct);
            }
            //huddi shu orderli id lik va product idlik orderProduct bo'lmas yangi orderProduct ochamiz
            else {
                Double price = productBasketDto.getPrice();
                OrderProduct orderProduct = new OrderProduct(
                        orderId,
                        productBasketDto.getId(),
                        CommonUtils.priceRoundDigit(price),
                        1
                );
                orderProductRepository.save(orderProduct);
            }
        }
        //Agar mahsulot o'chirish uchun shu order id lik va product idlik orderProduct bo'lishi kerak. Aks holda Throw
        else {
            OrderProduct orderProduct = orderProductRepository.findByOrderIdAndProductId(orderId, productBasketDto.getId()).orElseThrow(() ->
                    new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ORDER_PRODUCT_NOT_FOUND")));
            Integer count = orderProduct.getCount();
            if (count == 1) {
                orderProductRepository.delete(orderProduct);
            } else {
                Double price = (count - 1) * productBasketDto.getPrice();
                orderProduct.setCount(count - 1);
                orderProduct.setPrice(CommonUtils.priceRoundDigit(price));
                orderProductRepository.save(orderProduct);
            }
        }
    }

    //shu orderga hozirda productPriceBonus tushishi tekshiramiz va tushsa orderBonusga biriktiramiz
    private void checkProductPriceBonusAndUpdateOrderBonus(Double orderPrice, OrderBonus orderBonus) throws ParseException {
        Timestamp time = new Timestamp(new SimpleDateFormat("dd/MM/yyy hh:mm:ss").parse(kelishilganSana + " " + LocalTime.now()).getTime());
        Date date = Date.valueOf(LocalDate.now());

        //hozirda ushbu order price ga va sana va hozirgi vaqtga mos aksiya mavjudligini tekshiradi va bo'lsa shu ProductPriceBonus ni qaytaradi
        Optional<ProductPriceBonus> optionalProductPriceBonus = productPriceBonusRepository.getProductPriceBonusByCurrentTime(
                orderPrice,
                date,
                time,
                kelishilganSana,
                kelishilganSanaPlusKun);
        //Ushbu orderga ProductPriceBonus mavjud bolsa  va yangi bonus bo'lsa orderBonus ni o'zgartiramiz
        if (optionalProductPriceBonus.isPresent()) {
            ProductPriceBonus productPriceBonus = optionalProductPriceBonus.get();

            //Agar avval berilgan bonusdan farqli bo'lsa ushbu bonus id sini set qilamiz va boxName ni null qilamiz
            if (!productPriceBonus.getId().equals(orderBonus.getProductPriceBonusId())) {
                orderBonus.setProductPriceBonusId(productPriceBonus.getId());
                orderBonus.setProductPriceBonus(productPriceBonus);
                orderBonus.setBoxName(null);
            }
        }
        //agarda ushbu orderga bonus tushmasa orderBonus table ni ProductPriceBonus va boxName field lariga null beramiz
        else {
            orderBonus.setProductPriceBonus(null);
            orderBonus.setBoxName(null);
        }
    }

    //qaytaruvchi dtoni ushbu metod yordamizda yasaymiz
    private OrderBasketDto createApiResponse(Order order) {
        //ushbu order ning hamma savatchadagi mahsulotlarini idlarini bazadan olib keldik
        Set<String> stringProductIdsByOrderId = orderProductRepository.getProductIdsByOrderId(order.getId());
        Set<UUID> productIdsByOrderId = stringProductIdsByOrderId.stream().map(UUID::fromString).collect(Collectors.toSet());

        //feign orqali PRODUCT-SERVICE dan idlar orqali produclarni listini oldik
        List<ProductResDto> productResDtos = productFeign.getProductsByIds(productIdsByOrderId).getData();

        //OrderProduct table dan product idlari va countlarini Hashmapga yig`dik
        HashMap<String, Integer> stringProductIdAndCount = orderProductRepository.getProductIdAndCount(order.getId());
        Set<String> strings = stringProductIdAndCount.keySet();


        HashMap<UUID, Integer> productIdAndCount = new HashMap<>();
        for (String string : strings) {
            if (stringProductIdAndCount.containsKey(string))
                productIdAndCount.put(UUID.fromString(string), stringProductIdAndCount.get(string));
        }
        //productlar listi va hashmapni mapperga berib yuboramiz bizga productBasketDtos qaytaradi
        List<ProductBasketDto> productBasketDtos = productMapper.productListToProductBasketDtoList(productResDtos, productIdAndCount);

        return orderMapper.OrderToBasketDto(order, productBasketDtos);
    }

    //Ushbu metod yetkazib berish narxini tekshiradi va bo'lsa orderBonusga qo'shadi
    public void checkDeliveryFreeBonusAndUpdateOrderBonus(Double orderPrice, OrderBonus orderBonus) throws ParseException {
        Timestamp time = new Timestamp(new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(kelishilganSana + " " + LocalTime.now()).getTime());
        Date date = Date.valueOf(LocalDate.now());

        //Ushbu metod hozirda amalda bo'lgan aksiyani berilayotgan narx bonusdagi
        // narxdan katta yoki teng bo'lsa va shu aksiya hozirgi vaqtda amalda bo'lsa
        // tekshiradi
        Optional<DeliveryFreeBonus> optionalDeliveryFreeBonus = deliveryFreeBonusRepository.getDeliveryFreeBonusByCurrentTime(
                orderPrice,
                date,
                time,
                kelishilganSana,
                kelishilganSanaPlusKun
        );

        //optionalDeliveryFreeBonus mavjud bo'lsa, orderBonusga set qilamiz
        if (optionalDeliveryFreeBonus.isPresent()) {
            DeliveryFreeBonus deliveryFreeBonus = optionalDeliveryFreeBonus.get();
            orderBonus.setDeliveryFreeBonusId(deliveryFreeBonus.getId());
            orderBonus.setDeliveryFreeBonus(optionalDeliveryFreeBonus.get());

            //Agar mavjud bo'lmasa orderBonusga null beramiz
        } else {
            orderBonus.setDeliveryFreeBonus(null);
        }
    }

    // Bu metod buyurtmadagi productlarni sonini tekshiradi,
    // agar product soniga oid bonus mavjud bo'lsa, orderBonus ga
    // productCountBonus ni biriktiradi
    public void checkProductCountBonusAndUpdateOrderBonus(
            UUID orderId,
            OrderBonus orderBonus) throws ParseException {
        Timestamp time = new Timestamp(new SimpleDateFormat("dd/MM/yyy hh:mm:ss")
                .parse(kelishilganSana + " " + LocalTime.now()).getTime());
        Date date = Date.valueOf(LocalDate.now());

        // bu metod berilgan buyurtmadagi product lar va ularning sonini tekshirib,
        // mavjud bonuslardagi product sonidan katta yoki teng bo'lsa va hozirgi vaqt
        // aksiya amal qilish muddati ichida bo'lsa, shu aksiyani qaytaradi
        Optional<ProductCountBonus> optionalProductCountBonus = productCountBonusRepository
                .getProductCountBonusByCurrentTime(
                        orderId,
                        date,
                        time,
                        kelishilganSana,
                        kelishilganSanaPlusKun);
        // agar product soniga oid bonus mavjud bo'lsa, orderBonus ga
        // productCountBonus ni biriktiradi
        if (optionalProductCountBonus.isPresent())
            orderBonus.setProductCountBonus(optionalProductCountBonus.get());
            // aks xolda productCountBonusga null qiymat beradi
        else
            orderBonus.setProductCountBonus(null);
    }

    // Ushbu method berilgan orderga mos keladigan saleBonuslarni qidiradi va agar bulsa order ni price dan sale ning amount ni chegirib tashlab orderBonus ga ushbu saleBonusni set qiladi
    private void checkSaleBonusAndUpdateOrderBonus(Order order, OrderBonus orderBonus) throws ParseException {
        Timestamp time = new Timestamp(new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(kelishilganSana + " " + LocalTime.now()).getTime());
        Date date = Date.valueOf(LocalDate.now());

        // Ushbu method berilgan orderga mos keladigan saleBonuslarni qidiradi va agar bulsa order ni price dan sale ning amount ni chegirib tashlab orderBonus ga ushbu saleBonusni set qiladi
        Optional<SaleBonus> optionalSaleBonus = saleBonusRepository.getSaleBonusByCurrentTime(
                date,
                time,
                kelishilganSana,
                kelishilganSanaPlusKun,
                order.getPrice()
        );
        // agar berilgan vatda order dagi price ga mos saleBonus bulsa usha saleBonus ichidagi sale ning qiymati order dagi price dan chegirib tashlanadi
        // va yangi price order ning price siga setqilinadi hamda orderBonus ga ham sale Bonus set qilinadi
        if (optionalSaleBonus.isPresent()) {
            SaleBonus saleBonus = optionalSaleBonus.get();
            SaleResDto sale = productFeign.getOneSale(saleBonus.getSaleId()).getData();
            SaleTypeEnum type = sale.getType();

            double priceWithDiscount = order.getPrice();

            //agar sale ning typi percent bulsa order ning price sidan ushbu summa foiz hisobida chegiriladi
            if (type.equals(SaleTypeEnum.PERCENT)) {
                priceWithDiscount = order.getPrice() - (order.getPrice() * sale.getAmount() / 100);
            }

            //agar sale ning typi money bulsa order ning price sidan ushbu summa ning uzi chegiriladi
            else if (type.equals(SaleTypeEnum.MONEY)) {
                priceWithDiscount = order.getPrice() - sale.getAmount();
            }
            order.setPrice(CommonUtils.priceRoundDigit(priceWithDiscount));
            orderBonus.setSaleBonus(saleBonus);
        }
        // agar saleBonus yuq bulsa null quyiladi
        else {
            orderBonus.setSaleBonus(null);
        }
    }

    //order uchun code generatsiya qiladi
    private Long generateOrderCode() {
        //db dan oxirgi yaratilgan order code ni olib keladi
        Long orderCode = orderRepository.getOrderCodeByCreatedAt();
        //oxirgi yaratilgan order code ga 1 qo'shib yangi orderga biriktiramiz
        if (orderCode != null)
            return ++orderCode;
        return 1000L;
    }


    //Ushbu method user tomonidan uzining kutish statusidagi orderini bekor qilish uchun ishlatiladi
    @CheckAuth
    @Override
    public ApiResult<?> cancelOrderByCustomer(UUID orderId) {

        User user = CommonUtils.getUserFromRequest();
        Optional<Order> optionalOrder = orderRepository.findById(orderId);

        if (optionalOrder.isEmpty())
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ORDER_NOT_FOUND"));


        Order order = optionalOrder.get();

        //Statusi PENDING bulmagan orderlarni bekor qilish mumkin emas
        if (!order.getStatus().equals(OrderStatusEnum.PENDING))
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("CANCEL_ORDER_CUSTOMER"));

        //Sistemadagi userga tegishli bulmagan orderlarni bekor qilish mumkin emas
        if (!order.getCustomerId().equals(user.getId()))
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("ORDER_NOT_YOURS"));

        order.setStatus(OrderStatusEnum.CANCEL);
        orderRepository.save(order);

        //Order dagi status uzgarishini orderHistory ga yozib quyamiz
        OrderHistory orderHistory = new OrderHistory(
                order.getId(),
                user.getId(),
                order.getStatus()
        );

        orderHistoryRepository.save(orderHistory);

        return ApiResult.successResponse("Order bekor qilindi");

    }

    //Ushbu method oshxona tomonidan accept statusdagi orderni bekor qilish uchun ishlatiladi
    @CheckAuth
    @Override
    public ApiResult<?> rejectOrderByKitchen(UUID orderId) {
        User user = CommonUtils.getUserFromRequest();

        Order order = orderRepository.findById(orderId).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ORDER_NOT_FOUND")));

        //Status ACCEPTED ekanligini tekshiramiz
        if (!order.getStatus().equals(OrderStatusEnum.ACCEPTED))
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("BAD_REQUEST_ORDER_STATUS"));

        order.setStatus(OrderStatusEnum.REJECT);
        orderRepository.save(order);
        OrderHistory orderHistory = new OrderHistory(
                order.getId(),
                user.getId(),
                OrderStatusEnum.REJECT
        );
        orderHistoryRepository.save(orderHistory);
        return ApiResult.successResponse("");
    }

    //Ushbu method oshxona tomonidan kitchen statusdagi orderni ready qilish uchun ishlatiladi
    @CheckAuth
    @Override
    public ApiResult<?> readyOrderByKitchen(UUID orderId) {
        User user = CommonUtils.getUserFromRequest();

        Order order = orderRepository.findById(orderId).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ORDER_NOT_FOUND")));

        //Status KITCHEN ekanligini tekshiramiz
        if (!order.getStatus().equals(OrderStatusEnum.KITCHEN))
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("BAD_REQUEST_ORDER_STATUS"));

        order.setStatus(OrderStatusEnum.READY);
        orderRepository.save(order);
        OrderHistory orderHistory = new OrderHistory(
                order.getId(),
                user.getId(),
                OrderStatusEnum.READY
        );
        orderHistoryRepository.save(orderHistory);
        return ApiResult.successResponse(" ");
    }


    //Ushbu method oshxonaga branch ga tegishli bulgan , statusi KITCHEN bulgan orderlarning productlarini kursatadi
    @SneakyThrows
    @Override
    public ApiResult<?> viewOrdersByKitchen(UUID branchId) {

        //Branchga tegishli, KITCHEN status da bulgan order lar topildi
        List<Order> orders = orderRepository.findAllByStatusAndBranchIdOrderByCreatedAt(OrderStatusEnum.KITCHEN, branchId);

        //orderProduct larni orderId lar orqali topish uchun ularni list ga yigamiz
        List<UUID> orderIds = new ArrayList<>();

        //Oshxonaga malumot beruvchi orderKitchenViewDto lar List i yaratildi
        List<OrderKitchenViewDto> orderKitchenViewDtoList = new ArrayList<>();


        //order dagi productId lar orqali product larni boshqa servisdan olib kelish uchun ularni set ga yigamiz
        Set<UUID> productIds = new HashSet<>();


        //Order larni aylanib productId larni yigamiz
        for (Order order : orders) {
            if (order.getOrderBonus() != null) {
                if (order.getOrderBonus().getProductCountBonus() != null)
                    productIds.add(order.getOrderBonus().getProductCountBonus().getGiftProductId());

                if (order.getOrderBonus().getProductPriceBonus() != null) {
                    UUID productPriceBonusId = order.getOrderBonus().getProductPriceBonusId();
                    String boxName = order.getOrderBonus().getBoxName();
                    if (boxName != null) {
                        List<String> productIdInBonus = boxProductRepository.getProductIdsByBoxNameAndBonusId(boxName, productPriceBonusId);
                        List<UUID> uuidList = uuidFromString(productIdInBonus);
                        productIds.addAll(uuidList);
                    }
                }

            }

            orderIds.add(order.getId());
        }

        //Yuqoridagi topilgan orderId lar orqali productId lar topildi
        List<String> productIdsInOrder = orderProductRepository.getOrderProductIdsByOrderIdIn(orderIds);
        List<UUID> uuidList = uuidFromString(productIdsInOrder);
        productIds.addAll(uuidList);

        //Yuqoridagi topilgan productId lar orqali product lar topildi
        List<ProductResDto> productResDtoList = productFeign.getProductsByIds(productIds).getData();

        if (productResDtoList.size() != productIds.size()) {
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_NOT_FOUND"));
        }

        //orderProductlar orderId lar orqali topildi
        List<OrderProduct> orderProducts = orderProductRepository.findAllByOrderIdIn(orderIds);

        for (Order order : orders) {

            OrderKitchenViewDto orderKitchenViewDto =
                    new OrderKitchenViewDto(
                            order.getId(),
                            order.getPlanReceivedTime(),
                            order.getCreatedAt(),
                            new ArrayList<>(),
                            order.getCode()
                    );
            if (order.getOrderBonus() != null) {

                if (order.getOrderBonus().getProductCountBonus() != null) {

                    ProductKitchenViewDto productKitchenViewDto =
                            new ProductKitchenViewDto(
                                    order.getOrderBonus().getProductCountBonus().getGiftProductId(),
                                    getProductNameById(productResDtoList, order.getOrderBonus().getProductCountBonus().getGiftProductId()),
                                    order.getOrderBonus().getProductCountBonus().getGiftProductCount()
                            );

                    orderKitchenViewDto.getProducts().add(productKitchenViewDto);
                }
                if (order.getOrderBonus().getProductPriceBonus() != null) {
                    UUID productPriceBonusId = order.getOrderBonus().getProductPriceBonusId();

                    String boxName = order.getOrderBonus().getBoxName();
                    List<BoxProduct> boxProducts =
                            boxProductRepository.findAllByProductPriceBonusIdAndBoxName(productPriceBonusId,boxName);

                    for (BoxProduct boxProduct : boxProducts) {
                        ProductKitchenViewDto productKitchenViewDto =
                                new ProductKitchenViewDto(
                                        boxProduct.getProductId(),
                                        getProductNameById(productResDtoList, boxProduct.getProductId()),
                                        1
                                );

                        orderKitchenViewDto.getProducts().add(productKitchenViewDto);
                    }
                }
            }
            for (OrderProduct orderProduct : orderProducts) {
                if(order.getId().equals(orderProduct.getOrderId())){
                    ProductKitchenViewDto productKitchenViewDto=
                            new ProductKitchenViewDto(
                                    orderProduct.getProductId(),
                                    getProductNameById(productResDtoList, orderProduct.getProductId()),
                                    orderProduct.getCount()
                            );
                    orderKitchenViewDto.getProducts().add(productKitchenViewDto);
                }

            }
            orderKitchenViewDtoList.add(orderKitchenViewDto);
        }


        orderKitchenViewDtoList.sort(Comparator.comparing(OrderKitchenViewDto::getPlanReceivedTime)
                .thenComparing(OrderKitchenViewDto::getCreatedAt)
                .thenComparing(OrderKitchenViewDto::getOrderId));


        return ApiResult.successResponse(orderKitchenViewDtoList);
    }

    //Ushbu method product larning listidan productId orqali productName qaytaradi
    public String getProductNameById(List<ProductResDto> productResDtoList, UUID productId) {
        for (ProductResDto productResDto : productResDtoList) {
            if (productResDto.getId().equals(productId)) {
                return productResDto.getName();
            }
        }
        throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_NOT_FOUND"));
    }

    //Ushbu method String kurinishidagi uuid id larni UUID formatda cast qilib beradi
    private List<UUID> uuidFromString(List<String> list) {
        return list.stream().map(UUID::fromString).collect(Collectors.toList());
    }

    public CustomPage<OrderForOperatorDto> orderForOperatorDtoCustomPage(Page<Order> orderPage) {
        return new CustomPage<>(
                orderMapper.orderForOperatorDtoList(orderPage.getContent()),
                orderPage.getTotalPages(),
                orderPage.getNumber(),
                orderPage.getTotalElements(),
                orderPage.getSize(),
                orderPage.getNumberOfElements()
        );
    }


    //Biz CourierOrder bilan Courier bersak bizga courierOrderBoxDto yasab qaytaradi
    private CourierOrderBoxDto courierOrderToCourierOrderBoxDto(CourierOrder courierOrder, User user) {

        String[] orders = courierOrder.getOrders();
        List<UUID> orderIdList = Arrays.stream(orders).map(UUID::fromString).collect(Collectors.toList());
        List<Order> orderList = orderRepository.findAllByStatusAndIdIn(OrderStatusEnum.CLOSED, orderIdList);
        if (orderIdList.size() == orderList.size())
            return new CourierOrderBoxDto();
        List<OrderForCourierDto> orderForCourierDtoList = new ArrayList<>();
        List<UUID> addressIds = new ArrayList<>();
        List<UUID> customerIds = new ArrayList<>();
        orderList.forEach(order -> {
            addressIds.add(order.getAddressId());
            customerIds.add(order.getCustomerId());
        });
        List<AddressCourierDto> addressDtoList =addressFeign.addressesByIds(addressIds).getData();
        List<UserResDto> userResDtoList = authFeign.usersByIds(customerIds).getData();

        for (Order order : orderList) {
            UserResDto customer = getUserByIdFromList(userResDtoList, order.getCustomerId());
            AddressCourierDto address = getAddressByIdFromList(addressDtoList, order.getCustomerId());
            orderForCourierDtoList.add(
                    new OrderForCourierDto(
                            customer.getFirstName(),
                            customer.getLastName(),
                            customer.getPhoneNumber(),
                            new AddressDto(
                                    address.getLon(),
                                    address.getLat(),
                                    address.getFullAddress()
                            ),
                            order.getDeliveryPrice(),
                            order.getPrice(),
                            order.getPrice() + order.getDeliveryPrice()
                    )
            );
        }
        CourierOrderBoxDto courierOrderBoxDto = new CourierOrderBoxDto(
                user.getId(),
                orderForCourierDtoList
        );
        return courierOrderBoxDto;
    }




    //
    private UserResDto getUserByIdFromList(List<UserResDto> userResDtoList, UUID id){
        for (UserResDto userResDto : userResDtoList) {
            if (userResDto.getId().equals(id)){
                userResDtoList.remove(userResDto);
                return userResDto;
            }
        }
        return new UserResDto();
    }

    //
    private AddressCourierDto getAddressByIdFromList(List<AddressCourierDto> addressDtoList, UUID id){
        for (AddressCourierDto addressDto : addressDtoList) {
            if (addressDto.getId().equals(id)){
                addressDtoList.remove(addressDto);
                return addressDto;
            }
        }
        return new AddressCourierDto();
    }

    public CustomPage<ViewOrdersForClientDto> orderForClientDtoCustomPage(Page<Order> orderPage) {
        return new CustomPage<ViewOrdersForClientDto>(
                orderMapper.orderToViewClientDto(orderPage.getContent()),
                orderPage.getTotalPages(),
                orderPage.getNumber(),
                orderPage.getTotalElements(),
                orderPage.getSize(),
                orderPage.getNumberOfElements()
        );
    }


    //Customer va costumerId dan iborat hashMap yasash
    private HashMap<UUID, UserDto> createHashMapCustomerIdAndCustomer(List<UserDto> users) {
        HashMap<UUID, UserDto> customerIdAndCustomerHashMap = new HashMap<>();
        for (UserDto user : users) {
            customerIdAndCustomerHashMap.put(user.getId(), user);
        }
        return customerIdAndCustomerHashMap;
    }

    //Address va addressId dan iborat hashMap yasash
    private HashMap<UUID, AddressForDispatcherDto> createHashMapAddressIdAndAddress(List<AddressForDispatcherDto> addresses) {
        HashMap<UUID, AddressForDispatcherDto> addressIdAndAddressHashMap = new HashMap<>();
        for (AddressForDispatcherDto address : addresses) {
            addressIdAndAddressHashMap.put(address.getId(), address);
        }
        return addressIdAndAddressHashMap;
    }

    private HashMap<UUID, Integer> stringToUuidHashMap(HashMap<String, Integer> stringHashmap) {
        HashMap<UUID, Integer> hashMap = new HashMap<>();
        for (String s : stringHashmap.keySet()) {
            if (stringHashmap.containsKey(s)) {
                hashMap.put(UUID.fromString(s), stringHashmap.get(s));
            }
        }
        return hashMap;
    }

}
