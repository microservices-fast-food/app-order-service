package ai.ecma.apporderservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderForCourierDto {

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private AddressDto address;
    private Double deliveryPrice;
    private Double price;
    private Double totalPrice;
}
