package ai.ecma.apporderservice.config;


import ai.ecma.apporderservice.exception.RestException;
import ai.ecma.apporderservice.payload.ApiResult;
import com.google.common.io.CharStreams;
import com.google.gson.Gson;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

/**
 * Feign dan kelgan exception larni o'qib olish uchun yaratilgan CustomErrorDecoder class.
 */
public class CustomErrorDecoder implements ErrorDecoder {
    @Override
    public Exception decode(String methodKey, Response response) {
        String message = null;
        Reader reader = null;

        try {
            reader = response.body().asReader(StandardCharsets.UTF_8);
            //Easy way to read the stream and get a String object
            //Stream o'qish va String ob'ektini olishning oson usuli

            message = CharStreams.toString(reader);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //It is the responsibility of the caller to close the stream.
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Gson gson = new Gson();
        ApiResult<?> apiResult = gson.fromJson(message, ApiResult.class);
        return new RestException( HttpStatus.valueOf(response.status()),apiResult.getErrors());
    }
}
