package ai.ecma.apporderservice.service;

import ai.ecma.appdblib.entity.enums.OrderStatusEnum;
import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.order.Order;
import ai.ecma.appdblib.entity.order.Payment;
import ai.ecma.appdblib.entity.user.User;
import ai.ecma.appdblib.repository.order.OrderRepository;
import ai.ecma.appdblib.repository.order.PaymentRepository;
import ai.ecma.apporderservice.aop.annotation.CheckAuth;
import ai.ecma.apporderservice.component.MessageByLang;
import ai.ecma.apporderservice.exception.RestException;
import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.PayDto;
import ai.ecma.apporderservice.utils.CommonUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService{
    private final OrderRepository orderRepository;
    private final PaymentRepository paymentRepository;
    private final MessageByLang messageByLang;

    @CheckAuth(permission = PermissionEnum.DO_PAYMENT)
    @Override
    public ApiResult<?> paymentProcess(PayDto payDto) {
        Order order = orderRepository.findById(payDto.getOrderId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Order topilmadi"));
        Double orderTotalPrice = order.getPrice() + order.getDeliveryPrice();
        if (!orderTotalPrice.equals(payDto.getPrice()))
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("CONFLICT_ORDER_PRICE") );

        User user = CommonUtils.getUserFromRequest();
        Payment payment = new Payment(
                payDto.getOrderId(),
                payDto.getPrice(),
                user.getPaytypeId()
        );
        paymentRepository.save(payment);

        order.setStatus(OrderStatusEnum.ACCEPTED);
        orderRepository.save(order);

        return ApiResult.successResponse("OK");
    }
}
