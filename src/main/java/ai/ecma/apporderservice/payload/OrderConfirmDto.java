package ai.ecma.apporderservice.payload;

import ai.ecma.appdblib.entity.enums.OrderTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderConfirmDto {
    private UUID id;
    private OrderTypeEnum type;
    private UUID addressId;
    private UUID branchId;
    private Timestamp planReceivedTime;
    private Double distance;
    private Double deliveryPrice;
    private UUID payTypeId;
}
