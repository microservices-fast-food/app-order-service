package ai.ecma.apporderservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductForClientViewDto {
    private UUID id;
    private String name;
    private Double price;
    private Integer count;
    private String photoUrl;
    private boolean gift;
}
