package ai.ecma.apporderservice.service;

import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.bonus.*;

import java.util.List;
import java.util.UUID;

public interface ProductPriceBonusService {
    ApiResult<ProductPriceBonusBasketDto> getForBasket(UUID id);

    ApiResult<List<BoxProductForProductPriceBonusDto>> getBoxProductByProductPriceBonus(UUID id);

    ApiResult<List<ProductForBoxProductDto>> getProductForBoxProductClient(UUID productPriceBonusId, String name);

    ApiResult<ProductPriceBonusGetSelectDto> add(ProductPriceBonusAddDto productPriceBonusAddDto);

    ApiResult<ProductPriceBonusForAdminDto> edit(UUID id, ProductPriceBonusAddDto productPriceBonusAddDto);

    ApiResult<?> delete(UUID id);


}
