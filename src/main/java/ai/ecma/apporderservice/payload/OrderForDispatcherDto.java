package ai.ecma.apporderservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderForDispatcherDto {
    private UUID id;
    private String lastName;
    private String phoneNumber;
    private AddressForDispatcherDto address;
    private Double price;
    private Double deliveryPrice;
    private Double totalPrice;
    private Long code;
    private Timestamp planReceivedTime;
}
