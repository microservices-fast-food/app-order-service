package ai.ecma.apporderservice.feign;


import ai.ecma.appdblib.entity.user.User;
import ai.ecma.apporderservice.config.FeignConfig;
import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.BranchInfoForClientDto;
import ai.ecma.apporderservice.payload.FindNearestBranchDto;
import ai.ecma.apporderservice.payload.ProductBasketDto;
import ai.ecma.apporderservice.payload.bonus.ProductResDto;
import ai.ecma.apporderservice.payload.bonus.SaleBasketDto;
import ai.ecma.apporderservice.payload.bonus.SaleResDto;
import ai.ecma.apporderservice.utils.RestConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@FeignClient(name = RestConstants.PRODUCT_CONTROLLER, configuration = FeignConfig.class)
public interface ProductFeign {

    //product id lari to'plami orqali product lar listini ProductServisdan qaytaradi
    @GetMapping("/get-products-by-ids")
    ApiResult<List<ProductResDto>>getProductsByIds(@RequestBody Set<UUID> ids);

    @GetMapping("/auth/check-user")
    ApiResult<User> checkUser(@RequestHeader("Authorization") String token);

    @GetMapping("/{id}")
    ApiResult<ProductBasketDto> getProductById(@PathVariable(name = "id") UUID id);

    @GetMapping("/sale/{id}")
    ApiResult<SaleResDto> getOneSale(@PathVariable(name = "id")  UUID id);

    @GetMapping("/sale/{id}")
    ApiResult<SaleBasketDto> getOneSaleBasketDto(@PathVariable(name = "id")  UUID id);

    /**
     * Shartlar:
     * 1.Biz bergan address ga eng yaqin branchlar
     * 2.Biz bergan product lar shu branch larda bo'lsin
     * 3.Hozirgi vaqtda ushbu branchlar ish faolyatida bo'lsin
     * @param findNearestBranchDto
     * @return
     */
    @PostMapping("/find-nearest-branchs")
    ApiResult<List<BranchInfoForClientDto>> findNearestBranchs(@RequestBody FindNearestBranchDto findNearestBranchDto);

    /**
     * Shartlar:
     * 1.Biz bergan address ga eng yaqin branch
     * 2.Biz bergan product lar shu branch da bo'lsin
     * 3.Hozirgi vaqtda ushbu branch ish faolyatida bo'lsin
     * @param findNearestBranchDto
     * @return
     */
    @PostMapping("/find-nearest-branch")
    ApiResult<BranchInfoForClientDto> findNearestBranch(@RequestBody FindNearestBranchDto findNearestBranchDto);



}
