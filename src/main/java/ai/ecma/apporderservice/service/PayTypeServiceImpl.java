package ai.ecma.apporderservice.service;

import ai.ecma.appdblib.entity.enums.PayTypeEnum;
import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.order.PayType;
import ai.ecma.appdblib.repository.order.PayTypeRepository;
import ai.ecma.apporderservice.aop.annotation.CheckAuth;
import ai.ecma.apporderservice.component.MessageByLang;
import ai.ecma.apporderservice.exception.RestException;
import ai.ecma.apporderservice.mapper.PayTypeMapper;
import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.PayTypeResDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PayTypeServiceImpl implements PayTypeService{
    private final PayTypeRepository payTypeRepository;
    private final PayTypeMapper payTypeMapper;
    private final MessageByLang messageByLang;

    @CheckAuth(permission = PermissionEnum.VIEW_PAYTYPE)
    @Override
    public ApiResult<List<PayTypeResDto>> getAll() {
        List<PayType> payTypeList = payTypeRepository.findAll();
        return ApiResult.successResponse(payTypeMapper.payTypeListToResDto(payTypeList));
    }

    @CheckAuth(permission = PermissionEnum.VIEW_PAYTYPE)
    @Override
    public ApiResult<List<PayTypeResDto>> getFilter(PayTypeEnum type) {
        List<PayType> payTypeList;
        if (type.equals(PayTypeEnum.CARD)){
            payTypeList = payTypeRepository.findAllByPayType(PayTypeEnum.CARD);
        }else {
            payTypeList = payTypeRepository.findAllByPayType(PayTypeEnum.CASH);
        }
        return ApiResult.successResponse(payTypeMapper.payTypeListToResDto(payTypeList));
    }

    @CheckAuth(permission = PermissionEnum.VIEW_PAYTYPE)
    @Override
    public ApiResult<PayTypeResDto> getOne(UUID id) {
        PayType payType = payTypeRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND,messageByLang.getMessageByKey("PAY_TYPE_NOT_FOUND")));

        return ApiResult.successResponse(payTypeMapper.payTypeToResDto(payType));
    }
}
