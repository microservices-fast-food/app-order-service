package ai.ecma.apporderservice.service;

import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.order.bonus.ProductCountBonus;
import ai.ecma.appdblib.repository.order.ProductCountBonusRepository;
import ai.ecma.apporderservice.aop.annotation.CheckAuth;
import ai.ecma.apporderservice.component.MessageByLang;
import ai.ecma.apporderservice.exception.RestException;
import ai.ecma.apporderservice.feign.ProductFeign;
import ai.ecma.apporderservice.mapper.ProductCountBonusMapper;
import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.CustomPage;
import ai.ecma.apporderservice.payload.ProductBasketDto;
import ai.ecma.apporderservice.payload.bonus.*;
import ai.ecma.apporderservice.utils.CommonUtils;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@RequiredArgsConstructor
public class ProductCountBonusServiceImpl implements ProductCountBonusService {

    private final ProductCountBonusRepository productCountBonusRepository;
    private final ProductCountBonusMapper productCountBonusMapper;
    private final ProductFeign productFeign;
    private final MessageByLang messageByLang;

    @Value("${kelishilganSana}")
    private String kelishilganSana;
    @Value("${kelishilganSanaPlusKun}")
    private String kelishilganSanaPlusKun;

    @CheckAuth(permission = PermissionEnum.VIEW_BONUS)
    @SneakyThrows
    @Override
    public ApiResult<ProductCountBonusDisplayDto> addProductCountBonus(
            ProductCountBonusAddDto productCountBonusAddDto
    ) {
        // bu metod qo'shilayotgan aksiyaning
        // boshlanish sanasi uning tugash sanasidan keyin emasligini
        // tekshirib beradi
        CommonUtils.validateTimeAndDateOrThrow(
                productCountBonusAddDto.getStartDate(),
                productCountBonusAddDto.getEndDate(),
                productCountBonusAddDto.getStartTime(),
                productCountBonusAddDto.getEndTime()

        );

        //bu metod berilgan vaqt va sana oralig'idagi mavjud aksiyalarni
        // tekshiradi bo'lsa adminga qaytaradi
        checkConflictTimeProductCountBonus(
                productCountBonusAddDto.getStartTime(),
                productCountBonusAddDto.getEndTime(),
                productCountBonusAddDto.getStartDate(),
                productCountBonusAddDto.getEndDate(),
                null
        );

        //qabul qilingan productCountBonusAddDto ni
        // saqlash uchun productCountBonus obyektiga
        // mapper yordamida o'giriladi
        ProductCountBonus productCountBonus = productCountBonusMapper
                .productCountBonusAddDtoToProductCountBonus(productCountBonusAddDto);
        productCountBonusRepository.save(productCountBonus);
        return ApiResult.successResponse(
                productCountBonusMapper
                        .productCountBonusToProductCountBonusDisplayDto(productCountBonus)
        );
    }


    @Override
    public ApiResult<ProductCountBonusForBasketDto> getProductCountBonusForBasket(UUID id) {
        ProductCountBonus productCountBonus = productCountBonusRepository.findById(id)
                .orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_COUNT_BONUS_NOT_FOUND")));

        //topilgan productCountBonus ni Savatchadagi buyurtmalar ro'yxatida
        // ko'rsatish uchun kerakli field lari
        // (giftProductName, giftProductCount, photoUrl)ni
        // saqlovchi obyekt yaratiladi
        ProductCountBonusForBasketDto productCountBonusForBasketDto =
                productCountBonusToBasketDto(productCountBonus);
        return ApiResult.successResponse(productCountBonusForBasketDto);
    }

    @CheckAuth(permission = PermissionEnum.VIEW_BONUS)
    @Override
    public ApiResult<CustomPage<ProductCountBonusInfoDto>> getAllProductCountBonusesList(
            int page,
            int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<ProductCountBonus> productCountBonusPage = productCountBonusRepository.findAll(pageable);
        CustomPage<ProductCountBonusInfoDto> productCountBonusCustomPage =
                productCountBonusToCustomPage(productCountBonusPage);
        return ApiResult.successResponse(productCountBonusCustomPage);
    }

    @CheckAuth(permission = PermissionEnum.EDIT_BONUS)
    @SneakyThrows
    @Override
    public ApiResult<ProductCountBonusInfoDto> editProductCountBonus(
            ProductCountBonusAddDto productCountBonusAddDto,
            UUID id) {
        // bu metod qo'shilayotgan aksiyaning
        // boshlanish sanasi uning tugash sanasidan keyin emasligini
        // tekshirib beradi
        CommonUtils.validateTimeAndDateOrThrow(
                productCountBonusAddDto.getStartDate(),
                productCountBonusAddDto.getEndDate(),
                productCountBonusAddDto.getStartTime(),
                productCountBonusAddDto.getEndTime()
        );

        Date currentDate = new Date(System.currentTimeMillis());
        java.sql.Date startDate = productCountBonusAddDto.getStartDate();
        ProductCountBonus productCountBonus = productCountBonusRepository
                .findById(id).orElseThrow(
                        () -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_COUNT_BONUS_NOT_FOUND")));

        // Agar aksiya boshlangan bulsa faqat endDate field ini o'zgartirishimiz mumkin
        if (currentDate.after(startDate)) {

            //bu metod berilgan vaqt va sana oralig'idagi mavjud
            // aksiyalarni tekshiradi bo'lsa klientga qaytaradi
            checkConflictTimeProductCountBonus(
                    productCountBonus.getStartTime(),
                    productCountBonus.getEndTime(),
                    productCountBonus.getStartDate(),
                    productCountBonusAddDto.getEndDate(),
                    id
            );
            productCountBonus.setEndDate(productCountBonusAddDto.getEndDate());
            productCountBonusRepository.save(productCountBonus);

            ProductCountBonusInfoDto productCountBonusInfoDto = productCountBonusToInfoDto(productCountBonus);
            return ApiResult.successResponse(productCountBonusInfoDto);

            // Agar aksiya boshlanmagan bulsa barcha fieldlarini o'zgartirishimiz mumkin
        } else {
            //bu metod berilgan vaqt va sana oralig'idagi mavjud
            // aksiyalarni tekshiradi bo'lsa klientga qaytaradi
            checkConflictTimeProductCountBonus(
                    productCountBonusAddDto.getStartTime(),
                    productCountBonusAddDto.getEndTime(),
                    productCountBonusAddDto.getStartDate(),
                    productCountBonusAddDto.getEndDate(),
                    id
            );

            //qabul qilingan productCountBonusAddDto ning
            // o'zgargan field lari productCountBonus obyektiga
            // mapper yordamida set qilinadi
            productCountBonusMapper
                    .updateProductCountBonus(productCountBonusAddDto, productCountBonus);
            productCountBonusRepository.save(productCountBonus);
            ProductCountBonusInfoDto productCountBonusInfoDto = productCountBonusToInfoDto(productCountBonus);
            return ApiResult.successResponse(productCountBonusInfoDto);
        }
    }

    @CheckAuth(permission = PermissionEnum.DELETE_BONUS)
    @Override
    public ApiResult<?> deleteProductCountBonus(UUID id) {
        try {
            productCountBonusRepository.deleteById(id);
            return ApiResult.successResponse(messageByLang.getMessageByKey("DELETED"));
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_COUNT_BONUS_NOT_FOUND"));
        }
    }

    //bu metod berilgan vaqt va sana oralig'idagi mavjud aksiyalarni
    // tekshiradi bo'lsa adminga qaytaradi
    private void checkConflictTimeProductCountBonus(
            Time startTime,
            Time endTime,
            java.sql.Date startDate,
            java.sql.Date endDate,
            UUID id) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Date fixedDate = dateFormat.parse(kelishilganSana);
        Date fixedDayPlusOneDay = dateFormat.parse(kelishilganSanaPlusKun);
        long startTimeLong = fixedDate.getTime() + startTime.getTime();
        long endTimeLong = fixedDayPlusOneDay.getTime() + endTime.getTime();

        Timestamp startTimestamp = new Timestamp(startTimeLong);
        Timestamp endTimestamp = new Timestamp(endTimeLong);
        List<ProductCountBonus> existsByGivenTime;
        if (id != null)
            existsByGivenTime = productCountBonusRepository.getExistsByGivenTimeIdNot(
                    startDate,
                    endDate,
                    startTimestamp,
                    endTimestamp,
                    kelishilganSana,
                    kelishilganSanaPlusKun,
                    id
            );
        else
            existsByGivenTime = productCountBonusRepository.getExistsByGivenTime(
                    startDate,
                    endDate,
                    startTimestamp,
                    endTimestamp,
                    kelishilganSana,
                    kelishilganSanaPlusKun
            );

        if (!existsByGivenTime.isEmpty())
            throw new RestException(HttpStatus.CONFLICT, messageByLang.getMessageByKey("CONFLICT_TIME_AKSIYA"), existsByGivenTime);
    }


    public ProductCountBonusForBasketDto productCountBonusToBasketDto(
            ProductCountBonus productCountBonus
    ) {
        ProductBasketDto giftProduct = productFeign
                .getProductById(productCountBonus.getGiftProductId())
                .getData();
        return new ProductCountBonusForBasketDto(
                productCountBonus.getId(),
                giftProduct.getName(),
                productCountBonus.getGiftProductCount(),
                giftProduct.getPhotoUrl()
        );
    }

    private ProductCountBonusInfoDto productCountBonusToInfoDto(
            ProductCountBonus productCountBonus
    ) {
        ProductBasketDto product = productFeign
                .getProductById(productCountBonus.getProductId())
                .getData();
        ProductBasketDto giftProduct = productFeign
                .getProductById(productCountBonus.getGiftProductId())
                .getData();
        return new ProductCountBonusInfoDto(
                productCountBonus.getId(),
                productCountBonus.getName(),
                productCountBonus.getStartTime(),
                productCountBonus.getEndTime(),
                productCountBonus.getStartDate(),
                productCountBonus.getEndDate(),
                product.getName(),
                productCountBonus.getProductCount(),
                giftProduct.getName(),
                productCountBonus.getGiftProductCount(),
                giftProduct.getPhotoUrl());
    }

    private CustomPage<ProductCountBonusInfoDto> productCountBonusToCustomPage(
            Page<ProductCountBonus> productCountBonusPage) {
        List<ProductCountBonus> productCountBonusesList = productCountBonusPage.getContent();
        List<ProductCountBonusInfoDto> productCountBonusInfoDtoList =
                productCountBonusToInfoDtoList(productCountBonusesList);

        return new CustomPage<>(
                productCountBonusInfoDtoList,
                productCountBonusPage.getTotalPages(),
                productCountBonusPage.getNumber(),
                productCountBonusPage.getTotalElements(),
                productCountBonusPage.getSize(),
                productCountBonusPage.getNumberOfElements()
        );
    }

    private List<ProductCountBonusInfoDto> productCountBonusToInfoDtoList(
            List<ProductCountBonus> productCountBonusesList) {

        // productlarning listidagi productlarnnig id larini olib Set ga o'girib beradi
        Set<UUID> productIds = getAllProductIdsFromProductCountBonuses(productCountBonusesList);

        //product id lari to'plami orqali product lar listini ProductServisdan qaytaradi
        List<ProductResDto> productResDtoList = productFeign.getProductsByIds(productIds).getData();

        List<ProductCountBonusInfoDto> productCountBonusInfoDtoList = new ArrayList<>();

        for (ProductCountBonus productCountBonus : productCountBonusesList) {

            // berilgan product list ichidan id si berilgan id ga teng product ni qayatradi
            ProductResDto productById =
                    getProductById(productCountBonus.getProductId(), productResDtoList);

            // berilgan product list ichidan id si berilgan id ga teng product ni qayatradi
            ProductResDto giftProductById =
                    getProductById(productCountBonus.getGiftProductId(), productResDtoList);

            assert productById != null;
            assert giftProductById != null;
            productCountBonusInfoDtoList.add(
                    new ProductCountBonusInfoDto(
                            productCountBonus.getId(),
                            productCountBonus.getName(),
                            productCountBonus.getStartTime(),
                            productCountBonus.getEndTime(),
                            productCountBonus.getStartDate(),
                            productCountBonus.getEndDate(),
                            productById.getName(),
                            productCountBonus.getProductCount(),
                            giftProductById.getName(),
                            productCountBonus.getGiftProductCount(),
                            giftProductById.getPhotoUrl())
            );
        }
        return productCountBonusInfoDtoList;
    }

    // productlarning listidagi productlarnnig id larini olib Set ga o'girib beradi
    private Set<UUID> getAllProductIdsFromProductCountBonuses(
            List<ProductCountBonus> productCountBonusesList) {
        Set<UUID> productIds = new HashSet<>();
        productCountBonusesList.forEach((productCountBonus) -> {
                    productIds.add(productCountBonus.getProductId());
                    productIds.add(productCountBonus.getGiftProductId());
                }
        );
        return productIds;
    }

    // berilgan product list ichidan id si berilgan id ga teng product ni qayatradi
    private ProductResDto getProductById(
            UUID id,
            List<ProductResDto> productResDtoList) {
        for (ProductResDto productResDto : productResDtoList) {
            if (id == productResDto.getId())
                return productResDto;
        }
        return null;

    }



}
