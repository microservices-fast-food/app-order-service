package ai.ecma.apporderservice.service;

import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.order.bonus.DeliveryFreeBonus;
import ai.ecma.appdblib.repository.order.DeliveryFreeBonusRepository;
import ai.ecma.apporderservice.aop.annotation.CheckAuth;
import ai.ecma.apporderservice.component.MessageByLang;
import ai.ecma.apporderservice.exception.RestException;
import ai.ecma.apporderservice.mapper.DeliveryFreeBonusMapper;
import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.CustomPage;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusAddDto;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusBasketDto;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeBonusGetForAdmin;
import ai.ecma.apporderservice.payload.bonus.DeliveryFreeEditDto;
import ai.ecma.apporderservice.utils.CommonUtils;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DeliveryFreeBonusServiceImpl implements DeliveryFreeBonusService {

    private final DeliveryFreeBonusRepository deliveryFreeBonusRepository;

    private final DeliveryFreeBonusMapper deliveryFreeBonusMapper;

    private final MessageByLang messageByLang;

    @Value("${kelishilganSana}")
    private String kelishilganSana;
    @Value("${kelishilganSanaPlusKun}")
    private String kelishilganSanaPlusKun;

    @CheckAuth(permission = PermissionEnum.VIEW_BONUS)
    @Override
    public ApiResult<CustomPage<DeliveryFreeBonusBasketDto>> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<DeliveryFreeBonus> deliveryFreeBonusPage = deliveryFreeBonusRepository.findAll(pageable);
        CustomPage<DeliveryFreeBonusBasketDto> customPage = deliveryFreeBonusResDtoCustomPage(deliveryFreeBonusPage);
        return ApiResult.successResponse(customPage);
    }

    @CheckAuth(permission = PermissionEnum.VIEW_BONUS)
    @Override
    public ApiResult<CustomPage<DeliveryFreeBonusGetForAdmin>> getForAdmin(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<DeliveryFreeBonus> deliveryFreeBonusPage = deliveryFreeBonusRepository.findAll(pageable);
        CustomPage<DeliveryFreeBonusGetForAdmin> customPage = deliveryFreeBonusGetForAdminCustomPage(deliveryFreeBonusPage);
        return ApiResult.successResponse(customPage);
    }

    @CheckAuth(permission = PermissionEnum.VIEW_BONUS)
    @Override
    public ApiResult<DeliveryFreeBonusBasketDto> getOne(UUID id) {
        DeliveryFreeBonus deliveryFreeBonus = deliveryFreeBonusRepository.findById(id).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("DELIVERY_FREE_BONUS_NOT_FOUNT")));
        return ApiResult.successResponse(deliveryFreeBonusMapper.deliveryFreeBonusToResDto(deliveryFreeBonus));
    }

    @CheckAuth(permission = PermissionEnum.ADD_BONUS)
    @SneakyThrows
    @Override
    public ApiResult<DeliveryFreeBonusBasketDto> add(DeliveryFreeBonusAddDto deliveryFreeBonusAddDto) {

        //bu metod kiritilgan sana va vaqtlarni to'g'ri kiritilganligini tekshirib beradi
        CommonUtils.validateTimeAndDateOrThrow(deliveryFreeBonusAddDto.getStartDate(),
                deliveryFreeBonusAddDto.getEndDate(),
                deliveryFreeBonusAddDto.getStartTime(),
                deliveryFreeBonusAddDto.getEndTime()
        );

        //bu metod berilgan vaqt va sana oralig'idagi mavjud aksiyalarni tekshiradi bo'lsa klientga qaytaradi
        checkConflictTimeProductPriceBonus(
                deliveryFreeBonusAddDto.getStartTime(),
                deliveryFreeBonusAddDto.getEndTime(),
                deliveryFreeBonusAddDto.getStartDate(),
                deliveryFreeBonusAddDto.getEndDate(),
                null
        );
        DeliveryFreeBonus deliveryFreeBonus = deliveryFreeBonusMapper.deliveryFreeBonusTo(deliveryFreeBonusAddDto);
        deliveryFreeBonusRepository.save(deliveryFreeBonus);
        // Ushbu metod DeliveryFreeBonus dan DeliveryFreeBonusResDto ni yasab beradi
        DeliveryFreeBonusBasketDto deliveryFreeBonusBasketDto = deliveryFreeBonusMapper.deliveryFreeBonusToResDto(deliveryFreeBonus);
        return ApiResult.successResponse(deliveryFreeBonusBasketDto);
    }


    @CheckAuth(permission = PermissionEnum.EDIT_BONUS)
    @SneakyThrows
    @Override
    public ApiResult<DeliveryFreeBonusBasketDto> edit(DeliveryFreeEditDto deliveryFreeEditDto, UUID id) {
        CommonUtils.validateTimeAndDateOrThrow(
                deliveryFreeEditDto.getStartDate(),
                deliveryFreeEditDto.getEndDate(),
                deliveryFreeEditDto.getStartTime(),
                deliveryFreeEditDto.getEndTime()
        );

        Date currentDate = new Date(System.currentTimeMillis());
        java.sql.Date startDate = deliveryFreeEditDto.getStartDate();
        DeliveryFreeBonus deliveryFreeBonus = deliveryFreeBonusRepository.findById(id).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("DELIVERY_FREE_BONUS_NOT_FOUNT")));
        if (currentDate.after(startDate)) {
            deliveryFreeBonus.setEndDate(deliveryFreeEditDto.getEndDate());
            checkConflictTimeProductPriceBonus(
                    deliveryFreeBonus.getStartTime(),
                    deliveryFreeBonus.getEndTime(),
                    deliveryFreeBonus.getStartDate(),
                    deliveryFreeBonus.getEndDate(),
                    id
            );
            deliveryFreeBonusRepository.save(deliveryFreeBonus);
            DeliveryFreeBonusBasketDto deliveryFreeBonusBasketDto = deliveryFreeBonusMapper.deliveryFreeBonusToResDto(deliveryFreeBonus);
            return ApiResult.successResponse(deliveryFreeBonusBasketDto);
        } else
            checkConflictTimeProductPriceBonus(
                    deliveryFreeEditDto.getStartTime(),
                    deliveryFreeEditDto.getEndTime(),
                    deliveryFreeEditDto.getStartDate(),
                    deliveryFreeEditDto.getEndDate(),
                    id
            );
        // Ushbu metod deliveryFreeBonusReqDto dan olib deliveryFreeBonus ga qiymatlarini qo'yib beradi
        deliveryFreeBonusMapper.updateDeliveryFreeBonus(deliveryFreeEditDto, deliveryFreeBonus);
        deliveryFreeBonusRepository.save(deliveryFreeBonus);

        // Ushbu metod DeliveryFreeBonus dan DeliveryFreeBonusResDto ni yasab beradi
        DeliveryFreeBonusBasketDto deliveryFreeBonusBasketDto = deliveryFreeBonusMapper.deliveryFreeBonusToResDto(deliveryFreeBonus);
        return ApiResult.successResponse(deliveryFreeBonusBasketDto);
    }

    @CheckAuth(permission = PermissionEnum.DELETE_BONUS)
    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            deliveryFreeBonusRepository.deleteById(id);
            return ApiResult.successResponse(messageByLang.getMessageByKey("DELIVERY_FREE_BONUS_DELETED"));
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("DELIVERY_FREE_BONUS_NOT_FOUNT"));
        }
    }

    public CustomPage<DeliveryFreeBonusBasketDto> deliveryFreeBonusResDtoCustomPage(Page<DeliveryFreeBonus> deliveryFreeBonusPage) {
        return new CustomPage<>(
                deliveryFreeBonusMapper.deliveryFreeBonusToResDtoList(deliveryFreeBonusPage.getContent()),
                deliveryFreeBonusPage.getTotalPages(),
                deliveryFreeBonusPage.getNumber(),
                deliveryFreeBonusPage.getTotalElements(),
                deliveryFreeBonusPage.getSize(),
                deliveryFreeBonusPage.getNumberOfElements()
        );
    }

    public CustomPage<DeliveryFreeBonusGetForAdmin> deliveryFreeBonusGetForAdminCustomPage(Page<DeliveryFreeBonus> deliveryFreeBonusPage) {
        return new CustomPage<>(
                deliveryFreeBonusMapper.deliveryFreeBonusToResForAdminDto(deliveryFreeBonusPage.getContent()),
                deliveryFreeBonusPage.getTotalPages(),
                deliveryFreeBonusPage.getNumber(),
                deliveryFreeBonusPage.getTotalElements(),
                deliveryFreeBonusPage.getSize(),
                deliveryFreeBonusPage.getNumberOfElements()
        );
    }

    //bu metod berilgan vaqt va sana oralig'idagi mavjud aksiyalarni tekshiradi bo'lsa klientga qaytaradi
    private void checkConflictTimeProductPriceBonus(Time startTime, Time endTime,
                                                    java.sql.Date startDate, java.sql.Date endDate, UUID id) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date kelishilganSanaDate = dateFormat.parse(kelishilganSana);
        Date kelishilganSanaDatePlusBirKun = dateFormat.parse(kelishilganSanaPlusKun);
        long startTimeLong = kelishilganSanaDate.getTime() + startTime.getTime();
        long endTimeLong = kelishilganSanaDatePlusBirKun.getTime() + endTime.getTime();
        Timestamp startTimeStamp = new Timestamp(startTimeLong);
        Timestamp endTimeStamp = new Timestamp(endTimeLong);

        List<DeliveryFreeBonus> getExistsByGivenTime;
        if (id != null)
            getExistsByGivenTime = deliveryFreeBonusRepository.getExistsByGivenTimeNotId(
                    startDate,
                    endDate,
                    startTimeStamp,
                    endTimeStamp,
                    kelishilganSana,
                    kelishilganSanaPlusKun,
                    id
            );
        else
            getExistsByGivenTime = deliveryFreeBonusRepository.getExistsByGivenTime(
                    startDate,
                    endDate,
                    startTimeStamp,
                    endTimeStamp,
                    kelishilganSana,
                    kelishilganSanaPlusKun
            );

        if (!getExistsByGivenTime.isEmpty())
            throw new RestException(HttpStatus.CONFLICT, messageByLang.getMessageByKey("CONFLICT_AKSIYA"), getExistsByGivenTime);
    }


}
