package ai.ecma.apporderservice.service;

import ai.ecma.appdblib.entity.order.bonus.ProductCountBonus;
import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.CustomPage;
import ai.ecma.apporderservice.payload.bonus.ProductCountBonusAddDto;
import ai.ecma.apporderservice.payload.bonus.ProductCountBonusDisplayDto;
import ai.ecma.apporderservice.payload.bonus.ProductCountBonusForBasketDto;
import ai.ecma.apporderservice.payload.bonus.ProductCountBonusInfoDto;

import java.util.UUID;

public interface ProductCountBonusService {

    ApiResult<ProductCountBonusDisplayDto> addProductCountBonus(
            ProductCountBonusAddDto productCountBonusAddDto);

    ApiResult<ProductCountBonusForBasketDto> getProductCountBonusForBasket(UUID id);

    ApiResult<CustomPage<ProductCountBonusInfoDto>> getAllProductCountBonusesList(int page, int size);

    ApiResult<ProductCountBonusInfoDto> editProductCountBonus(
            ProductCountBonusAddDto productCountBonusAddDto,
            UUID id);

    ApiResult<?> deleteProductCountBonus(UUID id);

    ProductCountBonusForBasketDto productCountBonusToBasketDto(ProductCountBonus productCountBonus);

}
