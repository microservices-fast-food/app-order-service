package ai.ecma.apporderservice.payload.bonus;

import ai.ecma.appdblib.entity.enums.BonusTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SaleBonusViewDto {
    private UUID id;
    private Double minPrice;
    private String name;
    private BonusTypeEnum type;
    private SaleResDto sale;
}
