package ai.ecma.apporderservice.utils;

import ai.ecma.appdblib.entity.user.User;
import ai.ecma.apporderservice.component.BeanUtil;
import ai.ecma.apporderservice.component.DataLoader;
import ai.ecma.apporderservice.exception.RestException;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.sql.Time;
import java.util.Optional;
import java.util.UUID;

public class CommonUtils {

    //bu metod kiritilgan sana va vaqtlarni to'g'ri kiritilganligini tekshirib beradi
    public static void validateTimeAndDateOrThrow(Date startDate, Date endDate,
                                                  Time startTime, Time endTime) {
        //sanani tekshirish
        if (startDate.after(endDate)) {
            throw new RestException(HttpStatus.BAD_REQUEST, "Sana xato kiritildi");
        } else if (startDate.equals(endDate)) {
            if (startTime.after(endTime))
                throw new RestException(HttpStatus.BAD_REQUEST, "Aksiya kunini 1 kundan uzun kiriting");
        }

    }

    public static String buildPhotoUrl(UUID id) {
        return RestConstants.DOMAIN + RestConstants.ATTACHMENT_CONTROLLER + "/download/" + id;
    }

    //bu method hozir tizimdagi requestni headerini olish uchun
    public static HttpServletRequest currentRequest() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return Optional.ofNullable(servletRequestAttributes).map(ServletRequestAttributes::getRequest).orElse(null);
    }

    public static User getUserFromRequest() {
        try {

            HttpServletRequest httpServletRequest = currentRequest();
            return (User) httpServletRequest.getAttribute(RestConstants.REQUEST_ATTRIBUTE_CURRENT_USER);
        } catch (Exception e) {
            DataLoader dataLoader = BeanUtil.getBean(DataLoader.class);
            String dataLoaderMode = dataLoader.dataLoaderMode;
            if(dataLoaderMode.equals("always"))
                return null;

            throw new RestException(HttpStatus.UNAUTHORIZED, "UNAUTHORIZED");
        }
    }

    //bu method request headeridan tokenni olib beradi
    public static String getTokenFromRequest() {
        try {
            return currentRequest().getHeader("Authorization");
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }

    public static Double priceRoundDigit(Double price){
        return Math.round(price/100.0)*100.0;
    }
}
