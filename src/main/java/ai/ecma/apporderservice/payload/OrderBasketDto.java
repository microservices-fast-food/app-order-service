package ai.ecma.apporderservice.payload;

import ai.ecma.appdblib.entity.enums.OrderStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderBasketDto {
    private UUID id;
    private Double price;
    private OrderStatusEnum status;
    private UUID customerId;
    private List<ProductBasketDto> products;
    private OrderBonusBasketDto orderBonus;
}
