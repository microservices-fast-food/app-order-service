package ai.ecma.apporderservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddOrderDeliveryRate {
    @NotNull(message = "order id bo'sh bo'lmasligi kerak")
    private UUID orderId;

    @Positive(message = "Order Rate musbat bo'lishi kerak")
    private Integer orderRate;

    @Positive(message = "Delivery Rate musbat bo'lishi kerak")
    private Integer deliveryRate;
}
