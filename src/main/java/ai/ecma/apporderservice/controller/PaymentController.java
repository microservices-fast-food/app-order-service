package ai.ecma.apporderservice.controller;

import ai.ecma.apporderservice.payload.ApiResult;
import ai.ecma.apporderservice.payload.PayDto;
import ai.ecma.apporderservice.utils.RestConstants;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@RequestMapping(RestConstants.PAYMENT_CONTROLLER)
public interface PaymentController {
    @PostMapping()
    ApiResult<?> paymentProcess(@RequestBody @Valid PayDto payDto);
}
