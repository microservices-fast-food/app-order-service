package ai.ecma.apporderservice.mapper;


import ai.ecma.appdblib.entity.order.bonus.ProductPriceBonus;
import ai.ecma.appdblib.entity.order.bonus.SaleBonus;
import ai.ecma.apporderservice.payload.bonus.ProductPriceBonusBasketDto;
import ai.ecma.apporderservice.payload.bonus.SaleBonusResDto;
import ai.ecma.apporderservice.payload.bonus.SaleResDto;

public class CustomMapper {
    public static SaleBonusResDto saleBonusToDto(SaleBonus saleBonus, SaleResDto saleResDto) {
        return new SaleBonusResDto(
                saleBonus.getId(),
                saleBonus.getName(),
                saleBonus.getMinPrice(),
                saleResDto.getId()
        );
    }
    public static ProductPriceBonusBasketDto productPriceBonusGetForBasketToDto(ProductPriceBonus productPriceBonus){
        return new ProductPriceBonusBasketDto(
                productPriceBonus.getId(),
                productPriceBonus.getName()
        );
    }
}
